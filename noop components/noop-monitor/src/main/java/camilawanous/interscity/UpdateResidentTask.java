package camilawanous.interscity;

import lac.cnet.sddl.objects.Message;

import java.util.UUID;

public class UpdateResidentTask implements Runnable {

    private Monitor monitor;
    private Message message;

    public UpdateResidentTask(Monitor Monitor, Message Message){
        this.monitor = Monitor;
        this.message = Message;
    }

    @Override
    public void run() {

        final UUID residentId = this.monitor.residentByIds.get(message.getSenderId());

        if(residentId != null){
            //System.out.println("Resident already registered");
        }else{
            UUID UUIDisc = UUID.fromString(this.monitor.monitordata.addResident());

            this.monitor.residentByIds.put(message.getSenderId(), UUIDisc);

            //System.out.println("Resident registered: " + UUIDisc);
        }
        return;
    }
}
