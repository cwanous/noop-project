package noop.mobile;

import java.util.concurrent.ThreadLocalRandom;

public class ResidentMoveTask implements Runnable{

    private Resident resident;

    public ResidentMoveTask(Resident Resident){
        this.resident = Resident;
    }

    @Override
    public void run() {

        Integer MoveOrNot = ThreadLocalRandom.current().nextInt(2); // 10%
        if(MoveOrNot == 1){
            //System.out.println("### MOVE");

            Float StepX = (ThreadLocalRandom.current().nextFloat());
            Float StepY = (ThreadLocalRandom.current().nextFloat());

            //System.out.println("Steps X" + StepX.toString() +" - Y:" + StepY.toString());

            Integer MoreOrLess = ThreadLocalRandom.current().nextInt(2);

            if(MoreOrLess == 1){
                //System.out.println("Less");
                StepX = (-1)*StepX;
                StepY = (-1)*StepY;
            }

            if((StepX + this.resident.location.getLatitude()) < this.resident.GetResidentCityArea().MaxX() &&
                    ((StepX + this.resident.location.getLatitude())  > 0)){
                this.resident.location.IncreaseLatitude(StepX);
            }

            if((StepY + this.resident.location.getLongitude()) < this.resident.GetResidentCityArea().MaxY() &&
                    ((StepY + this.resident.location.getLongitude()) > 0)){
                this.resident.location.IncreaseLongitude(StepY);
            }

        }

        //System.out.println("# Resident Location: " + this.resident.location.toString());
        return;
    }

}
