package noop.mobile;

import camilawanous.noopdata.DataType;
import camilawanous.noopdata.NoopJASONData;
import lac.cnclib.sddl.message.ApplicationMessage;

public class SendLocationTask implements Runnable{

    private Resident resident;

    public SendLocationTask(Resident Resident){
        this.resident = Resident;
    }

    // Gets the Location and sends its to the Monitor
    @Override
    public void run() {

        // Message content
        NoopJASONData JSONTest = new NoopJASONData(DataType.LOCATIONRESIDENT);
        JSONTest.add("ID",this.resident.GetResidentId().toString());
        JSONTest.add("NowLocation", this.resident.location.toString());
        JSONTest.add("HomeLocation", this.resident.homeArea);

        try{
            // Creating message
            ApplicationMessage message = this.resident.CreateApplicationMessage(JSONTest);
            // Sending Message
            this.resident.SendMessageToMonitor(message);
        }catch (Exception e){
            e.printStackTrace();
        }

        System.out.println("# Resident Location: " + this.resident.location.toString());

        return;
    }
}
