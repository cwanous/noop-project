package camilawanous.core;

import components.GroupDefiner;
import components.groupselector.GroupSelector;

public class HelloPatrolGroupDefiner {

    public static void main(String[] args) {

        GroupSelector groupSelector = new HelloPatrolGroupSelector();
        new GroupDefiner(groupSelector);

        try {
            Thread.sleep(Long.MAX_VALUE);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
