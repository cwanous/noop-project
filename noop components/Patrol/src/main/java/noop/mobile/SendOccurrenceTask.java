package noop.mobile;

import camilawanous.noopdata.Location;
import camilawanous.noopdata.PatrolPosition;

import java.util.concurrent.ThreadLocalRandom;

public class SendOccurrenceTask implements Runnable{

    private Patrol patrol;

    public SendOccurrenceTask(Patrol patrol){
        this.patrol = patrol;
    }

    @Override
    public void run() {

        Integer SendOrNot = ThreadLocalRandom.current().nextInt(4); // 10%
        if(SendOrNot == 1 &&
                patrol.GetPatrolPosition() != PatrolPosition.ToOccurrence &&
                patrol.GetPatrolPosition() != PatrolPosition.OnOccurrence ){
            //System.out.println("### SEND OCC");
            Integer OccurrenceType = ThreadLocalRandom.current().nextInt(3) + 1; // 0, 1, 2
            Location OccurrenceLocation = this.patrol.patrolMove.GetRealTimeLocation();
            this.patrol.SendNoopJASONDataToMonitor(this.patrol.CreateOccurrence(OccurrenceType, OccurrenceLocation));

            System.out.println("### Send Occurrence Location " + this.patrol.patrolMove.GetRealTimeLocation().toString());
        }

        return;

    }
}
