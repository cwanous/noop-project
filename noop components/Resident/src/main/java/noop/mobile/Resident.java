package noop.mobile;

import camilawanous.noopdata.*;
import lac.cnclib.net.NodeConnection;
import lac.cnclib.net.NodeConnectionListener;
import lac.cnclib.net.groups.Group;
import lac.cnclib.net.groups.GroupCommunicationManager;
import lac.cnclib.net.groups.GroupMembershipListener;
import lac.cnclib.net.mrudp.MrUdpNodeConnection;
import lac.cnclib.sddl.message.ApplicationMessage;
import lac.cnclib.sddl.message.Message;
import lac.cnclib.sddl.serialization.Serialization;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Resident implements NodeConnectionListener, GroupMembershipListener {

    // Variables to locate the gateway, its IP address and port number
    private static String gatewayIP    = "127.0.0.1";
    private static int gatewayPort  = 5500;
    private MrUdpNodeConnection connection;

    // Variables to GroupMembershipListener trigger events
    private GroupCommunicationManager groupManager;
    private Group aGroup;

    // Thread Poll
    private final ScheduledThreadPoolExecutor threadPool;
    private static final int INTERVAL_IN_SECS = 10;
    private static final int INTERVAL_MOVE_IN_SECS = 5;

    // Location
    public String area;
    public Location location;
    public String homeArea;
    public ResidentMove residentMove; // only initiated after Connection
    public String residentArea = "";

    // ID
    private final UUID residentId;

    // City Area
    private final CityArea cityArea;

    public boolean OccurenceNotify;

    public Integer OccurrenceTypeDefault;

    public Resident(){

        InetSocketAddress address = new InetSocketAddress(gatewayIP, gatewayPort);

        this.residentId = UUID.randomUUID();

        // Starting City Area
        this.cityArea = new CityArea(9);
        SetHomeArea();

        // Starting location
        location = GetStartLocation();

        // Starting threadPool that handles Send and Get Location task
        this.threadPool = new ScheduledThreadPoolExecutor(100);
        this.threadPool.prestartAllCoreThreads();

        final SendLocationTask sendlocationtask = new SendLocationTask(this);
        final SendOccurrenceTask sendOccurrenceTask = new SendOccurrenceTask(this);
        final ResidentMoveTask residentMoveTask = new ResidentMoveTask(this);
        this.threadPool.scheduleWithFixedDelay(sendlocationtask, INTERVAL_IN_SECS, INTERVAL_IN_SECS, TimeUnit.SECONDS);
        this.threadPool.scheduleWithFixedDelay(sendOccurrenceTask, 2*INTERVAL_IN_SECS, INTERVAL_IN_SECS, TimeUnit.SECONDS);
        this.threadPool.scheduleWithFixedDelay(residentMoveTask, INTERVAL_MOVE_IN_SECS, INTERVAL_IN_SECS, TimeUnit.SECONDS);

        this.OccurenceNotify = false;

        // Connection MR-UDP
        try {
            // Create a connection object tha encapsulate the details and protocol involved in the physical connection with GW
            connection = new MrUdpNodeConnection();
            // Add a listener to the connection object which provides methods to react to the node GW connection
            connection.addNodeConnectionListener(this);
            // Establish the connection
            connection.connect(address);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public Resident(String Area, String OccurrenceType){

        InetSocketAddress address = new InetSocketAddress(gatewayIP, gatewayPort);

        this.residentId = UUID.randomUUID();

        this.OccurrenceTypeDefault = new Integer(OccurrenceType);

        // Starting City Area
        this.cityArea = new CityArea(4);
        SetHomeArea();

        // Starting location
        location = GetStartLocation(Area);

        // Starting threadPool that handles Send and Get Location task
        this.threadPool = new ScheduledThreadPoolExecutor(100);
        this.threadPool.prestartAllCoreThreads();

        final SendLocationTask sendlocationtask = new SendLocationTask(this);
        final SendOccurrenceTask sendOccurrenceTask = new SendOccurrenceTask(this);
        this.threadPool.scheduleWithFixedDelay(sendlocationtask, INTERVAL_IN_SECS, INTERVAL_IN_SECS, TimeUnit.SECONDS);
        this.threadPool.scheduleWithFixedDelay(sendOccurrenceTask, INTERVAL_IN_SECS, INTERVAL_IN_SECS, TimeUnit.SECONDS);

        this.OccurenceNotify = false;

        // Connection MR-UDP
        try {
            // Create a connection object tha encapsulate the details and protocol involved in the physical connection with GW
            connection = new MrUdpNodeConnection();
            // Add a listener to the connection object which provides methods to react to the node GW connection
            connection.addNodeConnectionListener(this);
            // Establish the connection
            connection.connect(address);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public Location GetStartLocation(String Area){
        String startAreaInt = Area;

        try {
            Location startLocation = this.cityArea.GetLocationFromArea(startAreaInt);
            return startLocation;
        }catch (Exception e){
            Location location = new Location();
            return location;
        }
    }


    public Location GetStartLocation(){
        Integer Max = this.cityArea.GetNumOfAreas() - 1;
        Integer startAreaInt = ThreadLocalRandom.current().nextInt(Max) + 1;

        try {
            Location startLocation = this.cityArea.GetLocationFromArea(startAreaInt.toString());
            return startLocation;
        }catch (Exception e){
            Location location = new Location();
            return location;
        }
    }


    public UUID GetResidentId(){
        return this.residentId;
    }


    public void SetHomeArea(){

        Integer AreaID = ThreadLocalRandom.current().nextInt(this.cityArea.GetNumOfAreas());
        homeArea = AreaID.toString();
    }


    public static void main(String[] args) {

        Logger.getLogger("").setLevel(Level.OFF);

        new Resident();
    }

    // Send Message to the Monitor
    public void SendMessageToMonitor(ApplicationMessage message){

        try {
            connection.sendMessage(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //System.out.println("Send Msg");
    }


    // Creates an ApplicationMessage from a JSONData
    public ApplicationMessage CreateApplicationMessage(NoopJASONData JSONData) throws IOException {
        // Creating message
        ApplicationMessage message = new ApplicationMessage();
        message.setContentObject(JSONData.toString());
        try {
            message.setContent(Serialization.toJavaByteStream(JSONData.toString()));
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
        return message;
    }

    @Override
    public void connected(NodeConnection nodeConnection) {

        System.out.println("# Connected - Home Area no " + this.homeArea + " - Location Now " + this.location.toString());

        // Inform Monitor that is online
        NoopJASONData jsonNotifyResident = new NoopJASONData(DataType.RESIDENT);
        jsonNotifyResident.add("ID",this.residentId.toString());
        jsonNotifyResident.add("AreaID",this.homeArea);

        SendNoopJASONDataToMonitor(jsonNotifyResident);

    }


    public void SendNoopJASONDataToMonitor(NoopJASONData JSONData){
        try{
            // Creating message
            ApplicationMessage message = this.CreateApplicationMessage(JSONData);
            // Sending Message
            this.SendMessageToMonitor(message);
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public NoopJASONData CreateOccurrence(Integer Type, Location Location){

        NoopJASONData JSONOccurrence = new NoopJASONData(DataType.OCCURRENCE);
        JSONOccurrence.add("IDResident",this.residentId.toString());
        JSONOccurrence.add("ID","");
        JSONOccurrence.add("Type",Type.toString());
        JSONOccurrence.add("Location",Location.toString());
        JSONOccurrence.add("Sit","Unsolved");

        return JSONOccurrence;
    }

    @Override
    public void reconnected(NodeConnection nodeConnection, SocketAddress socketAddress, boolean b, boolean b1) {

    }

    @Override
    public void disconnected(NodeConnection nodeConnection) {

    }

    @Override
    public void newMessageReceived(NodeConnection nodeConnection, Message message) {
        //System.out.println("### Message Received");

        // Get Message Content
        NoopJASONData JSONData = new NoopJASONData(Serialization.fromJavaByteStream(message.getContent()).toString());

        switch (JSONData.getType().toString()){
            case "OCCURRENCE": {
                System.out.println("### MESSAGE RECEIVED: Occurrence");

                NotifyUser(JSONData);
                break;
            }
            default:{
                break;
            }
        }

    }


    private void NotifyUser(NoopJASONData jsonData) {
        System.out.println("# OCCURRENCE on Area "+ jsonData.getByKey("AreaID") +
                " - Type " + jsonData.getByKey("Type") + " - Situation: " + jsonData.getByKey("Sit"));

    }

    @Override
    public void unsentMessages(NodeConnection nodeConnection, List<Message> list) {

    }

    @Override
    public void internalException(NodeConnection nodeConnection, Exception e) {

    }

    @Override
    public void enteringGroups(List<Group> list) {

    }

    @Override
    public void leavingGroups(List<Group> list) {

    }


    public CityArea GetResidentCityArea(){
        return this.cityArea;
    }


    public Location GetLocation(){
        return this.location;
    }

}
