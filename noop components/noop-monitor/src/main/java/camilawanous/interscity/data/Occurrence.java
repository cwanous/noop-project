package camilawanous.interscity.data;

import camilawanous.noopdata.Area;
import camilawanous.noopdata.Location;

import java.util.UUID;

public class Occurrence {

    private Integer type; // Represents the number of Patrols
    private Integer numberOfPatrols;
    private boolean situation;
    private Location location;
    private Area area;
    private UUID notifier;

    public Occurrence(Integer Type, Location Location, UUID Notifier, Area Area){
        this.type = Type;
        this.numberOfPatrols = 0;
        this.situation = false;
        this.location = Location;
        this.notifier = Notifier;
        this.area = Area;
    }

    public boolean Solved(){
        return this.situation;
    }

    public boolean PatrolFull(){
        if(this.numberOfPatrols >= type){
            return true;
        }else {
            return false;
        }
    }

    public void IncrementNP(){
        numberOfPatrols++;
    }

    public void SolveOccurrence(){
        this.situation = true;
    }

    public Area GetArea(){
        return this.area;
    }

    public Integer GetType(){
        return this.type;
    }

    public boolean HasResponse(){
        if(numberOfPatrols > 0){
            return true;
        }else{
            return false;
        }
    }

}
