package camilawanous.core;

import components.GroupDefiner;
import components.groupselector.GroupSelector;

public class HelloGroupDefiner {

    public static void main(String[] args) {

        GroupSelector groupSelector = new HelloGroupSelector();
        new GroupDefiner(groupSelector);

        try {
            Thread.sleep(Long.MAX_VALUE);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}