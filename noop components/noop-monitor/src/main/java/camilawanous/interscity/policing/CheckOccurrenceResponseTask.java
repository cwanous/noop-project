package camilawanous.interscity.policing;

import camilawanous.interscity.Monitor;
import camilawanous.interscity.data.Occurrence;
import camilawanous.noopdata.NoopJASONData;

public class CheckOccurrenceResponseTask implements Runnable{

    private Monitor monitor;

    private String uuidOccurrence;

    private NoopJASONData jsonOccurrenceData;

    public CheckOccurrenceResponseTask(Monitor Monitor,
                                       String Uuid,
                                       NoopJASONData JsonOccurrenceData){
        this.monitor = Monitor;
        this.uuidOccurrence = Uuid;
        this.jsonOccurrenceData = JsonOccurrenceData;
    }

    @Override
    public void run() {

        try{
            Thread.sleep(4000);

            final Occurrence occurrence = this.monitor.occurrenceByISCid.get(uuidOccurrence);

            if(occurrence != null){
                if(!occurrence.HasResponse()){
                    this.monitor.SendOccurrenceToAllPatrols(jsonOccurrenceData);
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        return;
    }
}
