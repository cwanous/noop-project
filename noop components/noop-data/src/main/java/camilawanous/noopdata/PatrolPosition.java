package camilawanous.noopdata;

public enum PatrolPosition {
    RequestArea,
    Hold,
    ToArea,
    OnArea,
    ToOccurrence,
    OnOccurrence;
}
