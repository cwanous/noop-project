package camilawanous.interscity;

import camilawanous.noopdata.NoopJASONData;
import lac.cnclib.sddl.message.ApplicationMessage;

import java.io.IOException;
import java.util.UUID;

/**
 * A Task that write Topics
 *
 *
 * @author Camila Wanous
 */

public class SendTopicTask implements Runnable{

    private Monitor monitor;
    private NoopJASONData data;
    private UUID togatewayId;
    private UUID tonodeId;


    public SendTopicTask(Monitor Monitor, NoopJASONData Data,
                         UUID ToGatewayId, UUID ToNodeId){
        this.monitor = Monitor;
        this.data = Data;
        this.togatewayId = ToGatewayId;
        this.tonodeId = ToNodeId;
    }

    @Override
    public void run() {
        try {
            ApplicationMessage messageReply = this.monitor.CreateApplicationMessage(this.data);
            this.monitor.writePrivateMessageTopic(
                    this.monitor.CreatePrivateMessage(messageReply, togatewayId, tonodeId));
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        return;
    }
}
