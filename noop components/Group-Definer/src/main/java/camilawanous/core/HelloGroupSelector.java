package camilawanous.core;

import camilawanous.noopdata.*;

import java.util.*;

import components.groupselector.GroupSelector;
import lac.cnclib.sddl.serialization.Serialization;
import lac.cnet.sddl.objects.GroupRegion;
import lac.cnet.sddl.objects.Message;

public class HelloGroupSelector implements GroupSelector {


    public class ResidentGroup{

        public Integer homeGroup;
        public Integer nowGroup;

        public ResidentGroup(Integer HomeGroup, Integer NowGroup){
            this.homeGroup = HomeGroup;
            this.nowGroup = NowGroup;
        }
    }

    Map<UUID, ResidentGroup> residentsById;
    Map<UUID, Integer> patrolsById;

    @Override
    public int getGroupType() {
        return 3; // Resident
    }

    public HelloGroupSelector(){
        this.residentsById = new HashMap<>();
        this.patrolsById = new HashMap<>();
    }

    @Override
    public Set<Integer> processGroups(Message nodeMessage) {

        CityArea cityArea = new CityArea(4);

        HashSet<Integer> groupCollection = new HashSet<Integer>(2, 1);

        NoopJASONData JSONData = new NoopJASONData(Serialization.fromJavaByteStream(nodeMessage.getContent()).toString());

        UUID ID = nodeMessage.getSenderId();

        if(JSONData.getType().equals("LOCATIONPATROL")){

            /*Location PatrolLocation = new Location(JSONData.getByKey("Location"));

            Area area = cityArea.GetAreaFromLocation(PatrolLocation);
            Integer areaInt = 0;
            try {
                areaInt = Integer.parseInt(area.getAreaID());
            }
            catch (NumberFormatException e) { }

            // Update Map
            final Integer patrolOldGroups = this.patrolsById.get(ID);

            this.patrolsById.put(ID, areaInt);

            groupCollection.add(areaInt);

            System.out.println("Location received from Patrol: "+ groupCollection.toString());*/

        }else if (JSONData.getType().equals("LOCATIONRESIDENT")) {

            String ResidentHomeLocation = JSONData.getByKey("HomeLocation");

            Location NowLocation = new Location(JSONData.getByKey("NowLocation"));

            Area areaNow = cityArea.GetAreaFromLocation(NowLocation);

            Integer areaHomeInt = 0;
            Integer areaNowInt = 0;
            try {
                areaHomeInt = Integer.parseInt(ResidentHomeLocation);
                areaNowInt = Integer.parseInt(areaNow.getAreaID());
            }
            catch (NumberFormatException e) { }

            ResidentGroup RG = new ResidentGroup(areaHomeInt, areaNowInt);
            this.residentsById.put(ID, RG);

            groupCollection.add(areaHomeInt);
            groupCollection.add(areaNowInt);

            System.out.println("Locations received from Resident "+ groupCollection.toString());

        }else{

            final Integer patrolOldGroups = this.patrolsById.get(ID);
            final ResidentGroup residentOldGroups = this.residentsById.get(ID);

            if(patrolOldGroups!=null){
                groupCollection.add(patrolOldGroups);
            }

            if(residentOldGroups!=null){
                groupCollection.add(residentOldGroups.homeGroup);
                groupCollection.add(residentOldGroups.nowGroup);
            }

            System.out.println("Different message received > keep Groups: "+ groupCollection.toString());

        }

        return groupCollection;
    }

    @Override
    public void createGroup(GroupRegion groupRegion) {

    }
}