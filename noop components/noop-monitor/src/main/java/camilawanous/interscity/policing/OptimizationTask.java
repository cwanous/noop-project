package camilawanous.interscity.policing;

import camilawanous.interscity.Monitor;
import camilawanous.interscity.data.MonitorPatrol;
import camilawanous.noopdata.DataType;
import camilawanous.noopdata.NoopJASONData;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class OptimizationTask implements Runnable{

    private Monitor monitor;

    public OptimizationTask(Monitor Monitor){
        this.monitor = Monitor;
    }

    public String GetCapabilityValueFromJASON(String StringJSON, String Capability){
        try{
            return new JSONObject(StringJSON).getJSONArray(Capability).getJSONObject(0).get("value").toString();
        }catch (Exception e){
            return "";
        }
    }

    @Override
    public void run() {

        try {

            System.out.println("### Start Optimization Task");
            // Get Areas from all Patrols
            if (this.monitor.GetPatrolByISCid() != null) {

                List<MonitorPatrol> monitorPatrolList = new ArrayList<MonitorPatrol>();

                for (Map.Entry<UUID, MonitorPatrol> entry : this.monitor.GetPatrolByISCid().entrySet()) {

                    String capabilities = this.monitor.monitordata.GetCapabilityLastValue(entry.getKey().toString());
                    String area = GetCapabilityValueFromJASON(capabilities, "AreaID");
                    String position = GetCapabilityValueFromJASON(capabilities, "Position");

                    MonitorPatrol monitorPatrol = new MonitorPatrol(entry.getValue(), area, position);

                    monitorPatrolList.add(monitorPatrol);
                }

                List<MonitorPatrol> newPatrolAreaMap = this.monitor.policing.ReCalculateAreaToAllPatrols(monitorPatrolList);

                if (newPatrolAreaMap != null) {
                    for (MonitorPatrol entry : newPatrolAreaMap) {

                        // Build and Send New Message
                        NoopJASONData JSONLocation = new NoopJASONData(DataType.AREA);
                        JSONLocation.add("AreaID", entry.area);
                        JSONLocation.add("Location", entry.location.toString());

                        // Send Position to Monitor
                        //this.monitor.SendAreaFromOptimization(JSONLocation, entry.gatewayId, entry.patrolId);
                        }

                    }
            }

        }catch (Exception e){
            System.out.println(e.toString());
        }
        System.out.println("### End OptimizationTask");
        return;

    }
}
