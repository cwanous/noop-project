package camilawanous.interscity.policing;

import camilawanous.interscity.Monitor;
import camilawanous.interscity.data.MonitorPatrol;
import camilawanous.noopdata.DefaultPatrol;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PatrolOptimizationTask implements Runnable{

    private Monitor monitor;

    public PatrolOptimizationTask(Monitor Monitor){
        this.monitor = Monitor;
    }

    @Override
    public void run() {

        System.out.println("### Start PatrolOptimizationTask");
        Map<UUID, DefaultPatrol> Patrols = new HashMap<UUID, DefaultPatrol>();

        for(Map.Entry<UUID, MonitorPatrol> entry: this.monitor.GetPatrolByISCid().entrySet()){
            try{

                DefaultPatrol defaultPatrol = new DefaultPatrol(entry.getValue().area, entry.getValue().position);
                Patrols.put(entry.getKey(), defaultPatrol);
            }catch (Exception e){

            }

        }

        if(Patrols.size()>0){

            DefaultPatrol PatrolAreaChanged = this.monitor.GetCityArea().PatrolOptimization(Patrols);
            System.out.println("### PatrolOptimization");

            if(PatrolAreaChanged != null){
                MonitorPatrol MonitorPatrolAreaChanged = this.monitor.GetPatrolByISCid().get(PatrolAreaChanged.patrolId);

                if(MonitorPatrolAreaChanged != null){
                    this.monitor.CreatesAreaMessage(MonitorPatrolAreaChanged, PatrolAreaChanged.area, PatrolAreaChanged.location);
                }
            }

        }

        return;

    }
}
