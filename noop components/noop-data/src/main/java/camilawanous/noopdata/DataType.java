package camilawanous.noopdata;

public enum DataType{
    RESIDENT,
    LOCATIONPATROL,
    LOCATIONRESIDENT,
    ID,
    AREA,
    OCCURRENCE,
    RETURN,
    POSITION;
}
