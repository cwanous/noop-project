package noop.mobile;

import java.util.concurrent.ThreadLocalRandom;

public class SolveOccurrenceTask implements Runnable{

    public Patrol patrol;

    public SolveOccurrenceTask(Patrol Patrol){
        this.patrol = Patrol;
    }

    @Override
    public void run() {

        // Time to solve
        Integer timeToSolve = ThreadLocalRandom.current().nextInt(10) + 15;
        //System.out.println(timeToSolve.toString() + " to solve Occurrence");
        try {
            Thread.sleep(4000); // 4.000 millis
            patrol.OccurrenceSolved();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return;

    }
}
