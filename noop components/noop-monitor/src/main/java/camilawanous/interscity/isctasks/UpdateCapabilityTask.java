package camilawanous.interscity.isctasks;

import camilawanous.interscity.Monitor;

import java.util.UUID;

public class UpdateCapabilityTask implements Runnable {

    private Monitor monitor;
    private UUID iscId;
    private String capability;
    private String value;
    private String time;

    public UpdateCapabilityTask(Monitor Monitor,
                                     UUID Uuid,
                                     String Capability,
                                     String Value,
                                     String Time){
        this.monitor = Monitor;
        this.iscId = Uuid;
        this.capability = Capability;
        this.value = Value;
        this.time = Time;

    }

    @Override
    public void run() {
        this.monitor.monitordata.updateCapability(iscId.toString(), capability, value, time);
        return;
    }
}
