package camilawanous.interscity.policing;

import camilawanous.interscity.Monitor;
import camilawanous.noopdata.NoopJASONData;
import lac.cnclib.net.groups.Group;
import lac.cnclib.sddl.message.ApplicationMessage;

import java.io.IOException;
import java.util.List;

public class SendGroupTopicTask implements Runnable {

    private Monitor monitor;
    private NoopJASONData data;
    private List<Group> groups;

    public SendGroupTopicTask(Monitor Monitor, NoopJASONData Data, List<Group> Groups){
        this.monitor = Monitor;
        this.data = Data;
        this.groups = Groups;
    }

    @Override
    public void run() {
        try {
            ApplicationMessage messageGroup = this.monitor.CreateApplicationMessage(this.data);
            for(Group group : this.groups){
                System.out.println("# Send Occurrence to Group: " + group.toString());
                this.monitor.writePrivateMessageTopic(
                        this.monitor.CreateGroupMessage(messageGroup, group));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return;
    }
}
