package noop.mobile;

import camilawanous.noopdata.Area;
import camilawanous.noopdata.Location;

import java.util.concurrent.CountDownLatch;

public class PatrolMoveTask extends Thread{

    private Area toArea;
    private Boolean onArea;
    private Boolean interreupted;
    private Location moveTaskLocation;
    private volatile boolean running = true;
    private PatrolMove patrolMove;
    private CountDownLatch latch;

    // Initiates receiving the Area it must go to and the RealTimeLocation
    public PatrolMoveTask(Area ToArea, PatrolMove PatrolMove, CountDownLatch Latch){
        this.toArea = ToArea;
        this.moveTaskLocation = PatrolMove.GetRealTimeLocation();
        this.onArea = false;
        this.interreupted = false;
        this.patrolMove = PatrolMove;
        this.latch = Latch;
    }

    public void Interrupted(){
        interreupted = true;
    }

    public void terminate() {
        running = false;
    }


    public void run() {
        while (running) {
            while(!onArea){
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Step();
                CheckLocation();
            }
            terminate();
        }

        if(this.onArea && !interreupted){
            this.patrolMove.Arrives();
        }

        latch.countDown();
        return;
    }


    private void CheckLocation() {
        if(EqualsFloat(this.toArea.getLat(),this.moveTaskLocation.getLatitude()) &&
                EqualsFloat(this.toArea.getLon(),this.moveTaskLocation.getLongitude())){
            this.onArea = true;
        }
    }

    // each 5 seconds a step
    // rate: 0.5 for step
    public void Step(){
        try{

            Float stepSize = 0.5f;

            if(!EqualsFloat(this.toArea.getLat(),this.moveTaskLocation.getLatitude())){

                if(this.toArea.getLat() > this.moveTaskLocation.getLatitude()){
                    this.moveTaskLocation.IncreaseLatitude(stepSize);

                }else {
                    this.moveTaskLocation.IncreaseLatitude(-stepSize);

                }

            }else if(!EqualsFloat(this.toArea.getLon(),this.moveTaskLocation.getLongitude())){

                if(this.toArea.getLon() > this.moveTaskLocation.getLongitude()){
                    this.moveTaskLocation.IncreaseLongitude(stepSize);

                }else {
                    this.moveTaskLocation.IncreaseLongitude(-stepSize);
                }
            }

            //System.out.println("Step: " + this.moveTaskLocation.toString());

        }catch (Exception e){
            System.out.println("# Patrol Stop Moving");
        }

    }


    public void ChangeArea(Area AreaChange){
        this.toArea = AreaChange;
    }


    public boolean EqualsFloat(Float F1, Float F2){
        if(Math.abs(F1-F2) < 0.000001f){
            return true;
        }
        return false;
    }


    // To get Location in real time
    public Location getLocation(){
        return this.moveTaskLocation;
    }

}
