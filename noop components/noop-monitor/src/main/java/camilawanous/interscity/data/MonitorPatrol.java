package camilawanous.interscity.data;

import camilawanous.noopdata.Location;
import camilawanous.noopdata.PatrolPosition;

import java.util.UUID;

public class MonitorPatrol{

    public UUID patrolId;
    public UUID gatewayId;
    public UUID iscId;

    public String area;
    public String position;
    public Location location;

    public MonitorPatrol(UUID senderId, UUID gatewayId, UUID uuiDisc) {
        this.patrolId = senderId;
        this.gatewayId = gatewayId;
        this.iscId = uuiDisc;
        this.position = PatrolPosition.RequestArea.toString();
    }

    public MonitorPatrol(MonitorPatrol monitorPatrol, String Area, String Position) {
        this.patrolId = monitorPatrol.patrolId;
        this.gatewayId = monitorPatrol.gatewayId;
        this.iscId = monitorPatrol.iscId;
        this.area = Area;
        this.position = Position;
    }
}
