package camilawanous.noopdata;

public class Area {

    private String ID;

    private Float lon;
    private Float lat;

    private Integer xMin;
    private Integer xMax;
    private Integer yMin;
    private Integer yMax;

    public Area(String ID,
                Integer XMin, Integer XMax, Integer YMin, Integer YMax
                ){

        this.ID = ID;

        this.xMin = XMin;
        this.xMax = XMax;
        this.yMin = YMin;
        this.yMax = YMax;

        this.lon = (float) (YMin+YMax)/2;
        this.lat = (float) (XMin+XMax)/2;

    }

    public String getAreaID(){
        return ID;
    }

    @Override
    public String toString(){

        return "AreaID: " + this.ID + " - Lon: " + this.lon + " - Lat: " + this.lat;
    }

    public Float getLon(){
        return this.lon;
    }

    public Float getLat(){
        return this.lat;
    }

    public Boolean InArea(Location L){
        if(L.getLatitude() >= this.xMin && L.getLatitude() <= this.xMax &&
            L.getLongitude() >= this.yMin && L.getLongitude() <= this.yMax){
            return true;
        }else{
            return false;
        }
    }

    public Location GetLocation(){
        Location AreaLocation = new Location(this.lat, this.lon);
        return AreaLocation;
    }
}
