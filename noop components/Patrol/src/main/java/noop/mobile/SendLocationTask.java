package noop.mobile;

import camilawanous.noopdata.DataType;
import camilawanous.noopdata.Location;
import camilawanous.noopdata.NoopJASONData;
import lac.cnclib.sddl.message.ApplicationMessage;

/**
 * A Task that sends Location to the Monitor
 *
 *
 * @author Camila Wanous
 */

public class SendLocationTask implements Runnable{

    private Patrol patrol;

    public SendLocationTask(Patrol patrol){
        this.patrol = patrol;
    }

    // Gets the Location and sends its to the Monitor
    @Override
    public void run() {

        // Message content
        NoopJASONData JSONTest = new NoopJASONData(DataType.LOCATIONPATROL);
        Location locNow = this.patrol.patrolMove.GetRealTimeLocation();

        try {
            JSONTest.add("ID",this.patrol.GetPatrolId().toString());
            JSONTest.add("Location", locNow.toString());
        }catch (Exception e){
            Location beginning = new Location();
            JSONTest.add("Location", beginning.toString());
        }

        try{
            // Creating message
            ApplicationMessage message = this.patrol.CreateApplicationMessage(JSONTest);
            // Sending Message
            this.patrol.SendMessageToMonitor(message);
        }catch (Exception e){
            e.printStackTrace();
        }

        System.out.println("# Patrol - Location: " + locNow.toString() + " Area: " + this.patrol.patrolArea);

        return;

    }
}
