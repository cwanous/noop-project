package camilawanous.interscity;

import camilawanous.interscity.data.MonitorPatrol;
import camilawanous.interscity.data.Occurrence;
import camilawanous.interscity.isctasks.UpdateCapabilityTask;
import camilawanous.interscity.policing.PolicingOptimization;
import camilawanous.interscity.policing.SendGroupTopicTask;
import camilawanous.noopdata.*;
import lac.cnclib.net.groups.Group;
import lac.cnclib.sddl.message.ApplicationMessage;
import lac.cnclib.sddl.serialization.Serialization;
import lac.cnet.sddl.objects.ApplicationObject;
import lac.cnet.sddl.objects.Message;
import lac.cnet.sddl.objects.PrivateMessage;
import lac.cnet.sddl.udi.core.SddlLayer;
import lac.cnet.sddl.udi.core.UniversalDDSLayerFactory;
import lac.cnet.sddl.udi.core.listener.UDIDataReaderListener;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class Monitor implements UDIDataReaderListener<ApplicationObject> {


    /**
     * InterSCity
     */
    public static ConnectionInterSCity connectionISC;
    public static MonitorData monitordata;

    /**
     * Policing Optimization
     */
    public static PolicingOptimization policing;
    private ScheduledExecutorService schedulePool;

    /**
     * ContextNet
     */
    SddlLayer core;

    /**
     * Map of InterSCity UUID X MonitorPatrol
     */
    private final Map<UUID, MonitorPatrol> patrolByISCid;

    /**
     * Map of Patrol UUID X MonitorPatrol
     */
    private final Map<UUID, MonitorPatrol> patrolByPatrolId;

    /**
     * Map of Residents UUID X InterSCity UUID
     */
    public final Map<UUID, UUID> residentByIds;

    /**
     * City Area
     */
    private CityArea cityArea;

    /**
     * Map of Registered Occurrence
     */
    public final Map<String, Occurrence> occurrenceByISCid;

    /**
     * Thread Pool Executor (handles Threads)
     */
    private ExecutorService threadPool;
    private static final int maximumPoolSize = Runtime.getRuntime().availableProcessors() * 2;

    /**
     * The constructor make the connection with the gateway
     */
    public Monitor() {

        this.patrolByISCid = new ConcurrentHashMap<UUID, MonitorPatrol>();
        this.patrolByPatrolId = new ConcurrentHashMap<UUID, MonitorPatrol>();
        this.residentByIds = new ConcurrentHashMap<UUID, UUID>();
        this.occurrenceByISCid = new ConcurrentHashMap<String, Occurrence>();

        // Threads
        this.threadPool = Executors.newFixedThreadPool(maximumPoolSize);
        this.schedulePool = Executors.newScheduledThreadPool(maximumPoolSize);
        // Changed 3: NOOPs
        //this.schedulePool.scheduleAtFixedRate(new PatrolOptimizationTask(this),24000, 12000, TimeUnit.MILLISECONDS);

        // InterSCity
        this.connectionISC = new ConnectionInterSCity();
        this.monitordata = new MonitorData(connectionISC);

        // Star Map Location
        policing = new PolicingOptimization();

        // Starting City Area
        this.cityArea = new CityArea(4);

        // ContextNet
        core = UniversalDDSLayerFactory.getInstance();
        core.createParticipant(UniversalDDSLayerFactory.CNET_DOMAIN);
        core.createPublisher();
        core.createSubscriber();

        // Data reader for the Message topic (application messages)
        Object receiveMessageTopic = core.createTopic(Message.class, Message.class.getSimpleName());
        core.createDataReader(this, receiveMessageTopic);

        // Data writer for the private message topic
        Object toMobileNodeTopic = core.createTopic(PrivateMessage.class, PrivateMessage.class.getSimpleName());
        core.createDataWriter(toMobileNodeTopic);

        System.out.println("===== Monitor Started =====");

        synchronized (this) {
            try {
                this.wait();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }


    public static void main(String[] args) throws Exception {

        new Monitor();

    }

    @Override
    public void onNewData(ApplicationObject applicationObject) {

        //System.out.println("### Received");

        // Parameter of this function: the message received
        Message message = (Message) applicationObject;
        // Get Message Content
        NoopJASONData JSONData = new NoopJASONData(Serialization.fromJavaByteStream(message.getContent()).toString());

        switch (JSONData.getType().toString()){
            case "AREA": {
                System.out.println("### AREA Received from " + message.getSenderId().toString());
                MonitorPatrol patrol = UpdateMonitorPatrol(message);
                ProcessAreaMessage(JSONData, patrol);
                break;
            }
            case "POSITION":{
                System.out.println("### POSITION Received from " + message.getSenderId().toString());
                MonitorPatrol patrol = UpdateMonitorPatrol(message);
                ProcessPositionMessage(JSONData, patrol);
                break;
            }
            case "RESIDENT":{
                System.out.println("### RESIDENT Received");
                threadPool.execute(new UpdateResidentTask(this, message));
                break;
            }
            case "OCCURRENCE":{
                System.out.println("### OCCURRENCE Received from " + message.getSenderId().toString());
                ProcessOccurrence(JSONData, message.getSenderId());
                break;
            }
            default:{
                break;
            }
        }

    }


    private void ProcessOccurrence(NoopJASONData jsonData, UUID SenderId) {

        String Situation = jsonData.getByKey("Sit");

        if(Situation.equals("Solved")){

            String PatrolReplyID = jsonData.getByKey("OccurrenceID");

            System.out.println("# Occurrence Solved " + PatrolReplyID);

            final Occurrence occurrence = this.occurrenceByISCid.get(PatrolReplyID);

            //Update Occurrence
            if(occurrence != null){
                occurrence.SolveOccurrence();
                this.occurrenceByISCid.put(PatrolReplyID, occurrence);
                jsonData.add("AreaID", occurrence.GetArea().getAreaID());
                jsonData.add("Type", occurrence.GetType().toString());
            }

            // Update Occurrence on ISC

            // Send Group Message to Residents
            List<Group> ResidentGroup = GetResidentGroupFromOccurrence(occurrence.GetArea().getAreaID());
            if(ResidentGroup != null){
                this.threadPool.execute(new SendGroupTopicTask(this, jsonData, ResidentGroup));
            }

        }else{
            // Get Occurrence Type
            Integer OccurrenceType = new Integer(jsonData.getByKey("Type"));
            // Get Occurrence Location
            Location OccurrenceLocation = new Location(jsonData.getByKey("Location"));
            // Find Area by the Occurrence Location
            Area OccurrenceArea = this.cityArea.GetAreaFromLocation(OccurrenceLocation);
            // Add Occurrence Area to the Occurrence Message
            jsonData.add("AreaID", OccurrenceArea.getAreaID());
            // Create a Occurrence
            Occurrence newOccurrence = new Occurrence(OccurrenceType, OccurrenceLocation, SenderId, OccurrenceArea);

            // Register Occurrence
            String UUIDisc = this.monitordata.addOccurrence();
            jsonData.add("ID", UUIDisc);

            // Insert Occurrence on local Map
            this.occurrenceByISCid.put(UUIDisc, newOccurrence);

            // Insert Occurrence on ISC

            // Send Group Message to Patrols (put more data into it0
            List<Group> groupList = GetPatrolGroupsFromOccurrence(OccurrenceArea.getAreaID());
            if(groupList != null){
                this.threadPool.execute(new SendGroupTopicTask(this, jsonData, groupList));
            }

            // Send Group Message to Residents
            List<Group> ResidentGroup = GetResidentGroupFromOccurrence(OccurrenceArea.getAreaID());
            if(ResidentGroup != null){
                this.threadPool.execute(new SendGroupTopicTask(this, jsonData, ResidentGroup));
            }

            System.out.println("# Occurrence Registered and Sent " + UUIDisc.toString() + " - Area: " + OccurrenceArea.getAreaID());

            // Changed 6: Scale
            //this.threadPool.execute(new CheckOccurrenceResponseTask(this, UUIDisc, jsonData));
        }


    }


    public void ProcessAreaMessage(NoopJASONData JSONData, MonitorPatrol patrol){

        // Calculate Area
        String PoliceArea = GetNewArea();

        // Get Area Location on InterSCity
        Location PoliceAreaLocation = new Location(0f,0f);

        // Update Area on InterSCity
        this.threadPool.execute(new UpdateCapabilityTask(this, patrol.iscId, "AreaID", PoliceArea, "2019-10-19"));

        // Update Area on patrolByPatrolId
        UpdatePatrolArea(patrol.patrolId, PoliceArea);

        // Build and Send New Message
        NoopJASONData JSONLocation = new NoopJASONData(DataType.AREA);
        JSONLocation.add("AreaID",PoliceArea);
        JSONLocation.add("Location",PoliceAreaLocation.toString());

        System.out.println("### Sending Area to "+ patrol.patrolId + " - Area: " + PoliceArea);
        // Send Position to Monitor
        this.threadPool.execute(new SendTopicTask(this,JSONLocation, patrol.gatewayId, patrol.patrolId));

    }

    // Changed 5
    public void ProcessPositionMessage(NoopJASONData JSONData, MonitorPatrol patrol){

        switch (JSONData.getByKey("Position")){
            case "ToArea":
            case "OnArea": {
                this.threadPool.execute(new UpdateCapabilityTask(this, patrol.iscId, "Position",
                        JSONData.getByKey("Position"), "2019-10-19"));
                UpdatePosition(patrol.patrolId, JSONData.getByKey("Position"));
                break;
            }
            case "ToOccurrence": {
                System.out.println("# Patrol moving To Occurrence");
                // Getting occurrence
                String PatrolReplyID = JSONData.getByKey("OccurrenceID");

                final Occurrence occurrence = this.occurrenceByISCid.get(PatrolReplyID);

                if(occurrence != null){
                    if(!occurrence.PatrolFull() && !occurrence.Solved()){
                        occurrence.IncrementNP();
                        this.occurrenceByISCid.put(PatrolReplyID, occurrence);
                    }else {
                        // Changed 5: GROUP - NOOPs
                        SendPatrolBackToArea(patrol);
                    }
                }else{
                    // Changed 5: GROUP - NOOPs
                    SendPatrolBackToArea(patrol);
                }
                break;
            }
            default:{

            }
        }

    }


    // Changed 4: GROUP - NOOPs
    private List<Group> GetPatrolGroupsFromOccurrence(String Area){

        List<Group> areasList = new ArrayList<>(100);
        Integer groupID = 0;

        try{
            List<Integer> prePositionsList = new ArrayList<>(100);

            Integer areasPerSide = this.cityArea.GetAreasPerSide();

            groupID = Integer.parseInt(Area);
            prePositionsList.add(groupID);

            Integer groupIDRight = groupID + 1;
            if(((groupID-1)/areasPerSide) == ((groupIDRight-1)/areasPerSide)){
                prePositionsList.add(groupIDRight);
            }

            Integer groupIDLeft = groupID - 1;
            if(((groupID-1)/areasPerSide) == ((groupIDLeft-1)/areasPerSide)){
                prePositionsList.add(groupIDLeft);
            }

            Integer groupIDUP = groupID + this.cityArea.GetAreasPerSide();
            prePositionsList.add(groupIDUP);

            Integer groupIDUPRight =  groupIDUP + 1;
            if(((groupIDUP-1)/areasPerSide) == ((groupIDUPRight-1)/areasPerSide)){
                prePositionsList.add(groupIDUPRight);
            }

            Integer groupIDUPLeft =  groupIDUP - 1;
            if(((groupIDUP-1)/areasPerSide) == ((groupIDUPLeft-1)/areasPerSide)){
                prePositionsList.add(groupIDUPLeft);
            }

            Integer groupIDDOWN = groupID - this.cityArea.GetAreasPerSide();
            prePositionsList.add(groupIDDOWN);

            Integer groupIDDOWNRight =  groupIDDOWN + 1;
            if(((groupIDDOWN-1)/areasPerSide) == ((groupIDDOWNRight-1)/areasPerSide)){
                prePositionsList.add(groupIDDOWNRight);
            }

            Integer groupIDDOWNLeft =  groupIDDOWN - 1;
            if(((groupIDDOWN-1)/areasPerSide) == ((groupIDDOWNLeft-1)/areasPerSide)){
                prePositionsList.add(groupIDDOWNLeft);
            }


            for(Integer j: prePositionsList){
                if( j > 0 && j <= this.cityArea.GetNumOfAreas()){
                    Group group = new Group(2,j);
                    areasList.add(group);
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        return areasList;

    }

    // Changed 4: DEFAULT
    /*private List<Group> GetPatrolGroupsFromOccurrence(String Area){

        List<Group> areasList = new ArrayList<>(100);
        Integer groupID = 0;

        try{
            List<Integer> prePositionsList = new ArrayList<>(100);

            Integer totalAreas = this.cityArea.GetNumOfAreas();

            for(int i = 1; i <= totalAreas; i++){
                prePositionsList.add(i);
            }

            for(Integer j: prePositionsList){
                if( j > 0 && j <= this.cityArea.GetNumOfAreas()){
                    Group group = new Group(2,j);
                    areasList.add(group);
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        return areasList;

    }*/

    // Changed 4: Scale
    /*private List<Group> GetPatrolGroupsFromOccurrence(String Area){

        List<Group> areasList = new ArrayList<>(100);
        Integer groupID = Integer.parseInt(Area);
        Group group = new Group(2, groupID);
        areasList.add(group);

        return areasList;

    }*/


    private List<Group> GetResidentGroupFromOccurrence(String Area){

        List<Group> areasList = new ArrayList<>(100);
        Integer groupID = 0;

        try{
            groupID = Integer.parseInt(Area);
            Group group = new Group(3,groupID);
            areasList.add(group);

        }catch (Exception e){
            e.printStackTrace();
        }

        return areasList;
    }


    private List<Group> GetAllPatrolGroups(){

        List<Group> areasList = new ArrayList<>(100);

        try{
            List<Integer> prePositionsList = new ArrayList<>(100);

            Integer totalAreas = this.cityArea.GetNumOfAreas();

            for(int i = 1; i <= totalAreas; i++){
                prePositionsList.add(i);
            }

            for(Integer j: prePositionsList){
                if( j > 0 && j <= this.cityArea.GetNumOfAreas()){
                    Group group = new Group(2,j);
                    areasList.add(group);
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        return areasList;

    }


    /**
     * register Patrol on InterSCity
     */
    private MonitorPatrol UpdateMonitorPatrol(Message message) {

        final MonitorPatrol messagePatrol = this.patrolByPatrolId.get(message.getSenderId());

        if (messagePatrol != null) {
            //System.out.println("Patrol already registered");
        }else{

            UUID UUIDisc = UUID.fromString(this.monitordata.addPatrol(0f, 0f));

            MonitorPatrol patrol = new  MonitorPatrol(message.getSenderId(), message.getGatewayId(), UUIDisc);

            this.patrolByPatrolId.put(message.getSenderId(), patrol);
            this.patrolByISCid.put(UUIDisc, patrol);

            System.out.println("# Patrol registered: " + UUIDisc);
        }

        return this.patrolByPatrolId.get(message.getSenderId());

    }


    private String GetNewArea(){

        System.out.println("# Optimization: Getting best new Area");

        Map<UUID, DefaultPatrol> Patrols = new HashMap<UUID, DefaultPatrol>();

        for(Map.Entry<UUID, MonitorPatrol> entry: this.patrolByPatrolId.entrySet()){
            try{
                DefaultPatrol defaultPatrol = new DefaultPatrol(entry.getValue().area, entry.getValue().position);
                Patrols.put(entry.getKey(), defaultPatrol);
            }catch (Exception e){

            }
        }

        return this.cityArea.CalculateNewPatrolArea(Patrols);
    }


    private void UpdatePatrolArea(UUID patrolId, String policeArea) {
        final MonitorPatrol messagePatrol = this.patrolByPatrolId.get(patrolId);
        if(messagePatrol != null){
            messagePatrol.area = policeArea;
            this.patrolByPatrolId.put(patrolId, messagePatrol);
        }
    }


    public void CreatesAreaMessage(MonitorPatrol patrol, String Area, Location Location){

        System.out.println("# Sending New Area to " + patrol.patrolId);

        // Calculate Area
        String PoliceArea = Area;

        // Get Area Location on InterSCity
        Location PoliceAreaLocation = Location;

        // Update Area on InterSCity
        this.threadPool.execute(new UpdateCapabilityTask(this, patrol.iscId,
                "AreaID", PoliceArea, "2019-10-27"));

        // Update Area on patrolByPatrolId
        UpdatePatrolArea(patrol.patrolId, PoliceArea);

        // Build and Send New Message
        NoopJASONData JSONLocation = new NoopJASONData(DataType.AREA);
        JSONLocation.add("AreaID",PoliceArea);
        JSONLocation.add("Location",PoliceAreaLocation.toString());

        // Send Position to Monitor
        this.threadPool.execute(new SendTopicTask(this,JSONLocation, patrol.gatewayId, patrol.patrolId));

    }


    public void SendPatrolBackToArea(MonitorPatrol patrol){

        System.out.println("# Sending Patrol RETURN");
        NoopJASONData JSONLocation = new NoopJASONData(DataType.RETURN);
        this.threadPool.execute(new SendTopicTask(this,JSONLocation, patrol.gatewayId, patrol.patrolId));

    }


    public void UpdatePosition(UUID PatrolID, String Position){
        final MonitorPatrol messagePatrol = this.patrolByPatrolId.get(PatrolID);
        if(messagePatrol != null){
            messagePatrol.position = Position;
            this.patrolByPatrolId.put(PatrolID, messagePatrol);
        }
    }


    // Creates an ApplicationMessage from a JSONData
    public ApplicationMessage CreateApplicationMessage(NoopJASONData JSONData) throws IOException {

        // Creating message
        ApplicationMessage message = new ApplicationMessage();
        message.setContentObject(JSONData.toString());
        try {
            message.setContent(Serialization.toJavaByteStream(JSONData.toString()));
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
        return message;

    }


    public PrivateMessage CreatePrivateMessage(ApplicationMessage appMsg,
                                     UUID GatewayId,
                                     UUID NodeId){
        // Creating a Private Message (Data writer) to be sent as a reply to the NM
        PrivateMessage privateMessage = new PrivateMessage();
        privateMessage.setGatewayId(GatewayId);
        privateMessage.setNodeId(NodeId);
        privateMessage.setMessage(Serialization.toProtocolMessage(appMsg));

        return privateMessage;
    }

    public void writePrivateMessageTopic(PrivateMessage privateMessage){
        core.writeTopic(PrivateMessage.class.getSimpleName(), privateMessage);
    }


    public PrivateMessage CreateGroupMessage(ApplicationMessage appMsg, Group group){

        PrivateMessage groupMessage = new PrivateMessage();

        groupMessage.setGatewayId(UniversalDDSLayerFactory.BROADCAST_ID);
        groupMessage.setNodeId(UniversalDDSLayerFactory.BROADCAST_ID);
        groupMessage.setGroupId(group.getGroupID());
        groupMessage.setGroupType(group.getGroupType());
        groupMessage.setMessage(Serialization.toProtocolMessage(appMsg));


        return groupMessage;
    }


    public Map<UUID, MonitorPatrol> GetPatrolByISCid(){
        return this.patrolByISCid;
    }


    public CityArea GetCityArea(){
        return this.cityArea;
    }


    public Map<UUID, MonitorPatrol> GetPatrolByPatrolId(){
        return this.patrolByPatrolId;
    }


    public void SendOccurrenceToAllPatrols(NoopJASONData jsonOccurrenceData){
        List<Group> groupList = GetAllPatrolGroups();
        if(groupList != null){
            this.threadPool.execute(new SendGroupTopicTask(this, jsonOccurrenceData, groupList));
        }
    }
}
