package camilawanous.interscity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MonitorData {

    public static ConnectionInterSCity connectionISC;

    public MonitorData(ConnectionInterSCity connection){
        this.connectionISC = connection;
    }

    // Already added
    //String defaultcapabilities = addDefaultCapabilities();

    //String catalog = addResident(3.0f,3.0f);
    //System.out.println(catalog);

            /* updateCapability("e6a75eb6-3be6-4c96-a211-c4577e46a435",
                    "Situation", "Solved", "2019-06-11");
             */

    //updateLocation("e6a75eb6-3be6-4c96-a211-c4577e46a435", 2.0f,2.0f);
    //updateLocation("ed854e9a-fcbf-425f-a145-cbcffc4e8aed", "10", "12" ,"2019-10-11");

    //String LastValue = GetCapabilityLastValue("ed854e9a-fcbf-425f-a145-cbcffc4e8aed", "Longitude");
    //System.out.println(LastValue);

    public static String getCapabilities() throws Exception {
        try{
            return connectionISC.sendGet("catalog/capabilities","");
        }catch (Exception e){
            return "";
        }
    }

    // Update one capability with a new value and the collection time
    public static void updateCapability(String uuid, String Capability, String Value, String Time){
        // building POST directory
        String directory = "adaptor/resources/" + uuid + "/data";

        // building JSON
        JSONObject json = new JSONObject();
        json.put("value", Value);
        json.put("date", Time);

        JSONArray arrayOnJason = new JSONArray();
        arrayOnJason.put(json);

        JSONObject capability = new JSONObject();
        capability.put(Capability, arrayOnJason);

        JSONObject data = new JSONObject();
        data.put("data",capability);

        //System.out.println(data.toString());

        try{
            connectionISC.sendPost(directory, data.toString());
        }catch (Exception e){
            System.out.println(e);
        }

        System.out.println("--- ISC: Capability updated > "+ Capability);

    }

    // Update Location (Latitude and Longitude Capabilities)
    public static void updateLocation(String uuid, String Lat, String Lon, String Time){
        // building POST directory
        String directory = "adaptor/resources/" + uuid + "/data";

        // building JSON
        JSONObject jsonLat = new JSONObject();
        jsonLat.put("value", Lat);
        jsonLat.put("date", Time);

        JSONArray arrayOnJasonLat = new JSONArray();
        arrayOnJasonLat.put(jsonLat);

        //JSONObject capabilityLat = new JSONObject();
        //capabilityLat.put("Latitude", arrayOnJasonLat);

        JSONObject jsonLon = new JSONObject();
        jsonLon.put("value", Lon);
        jsonLon.put("date", Time);

        JSONArray arrayOnJasonLon = new JSONArray();
        arrayOnJasonLon.put(jsonLon);

        JSONObject capability = new JSONObject();
        capability.put("Longitude", arrayOnJasonLon);
        capability.put("Latitude", arrayOnJasonLat);

        JSONObject data = new JSONObject();
        data.put("data",capability);

        System.out.println(data.toString());

        try{
            connectionISC.sendPost(directory, data.toString());
        }catch (Exception e){
            System.out.println(e);
        }

    }

    // Update values
    public static void updateCapabilities(String uuid){
        // building POST directory
        String directory = "adaptor/resources/" + uuid + "/data";

        // building JSON
        JSONObject json = new JSONObject();
        json.put("value", "1");
        json.put("date", "2019-06-21T23:27:35.000Z");

        JSONArray ara = new JSONArray();
        ara.put(json);

        JSONObject capability = new JSONObject();
        capability.put("AreaSize",ara);

        JSONObject data = new JSONObject();
        data.put("data",capability);

        System.out.println(data.toString());

        try{
            connectionISC.sendPost(directory, data.toString());
        }catch (Exception e){
            System.out.println(e);
        }

    }

    // Update location by the uuid
    public static void updateLocation(String uuid, Float lat,  Float lon){
        // building POST directory
        String directory = "catalog/resources/" + uuid;

        // building JSON
        JSONObject json = new JSONObject();
        json.put("lat", lat);
        json.put("lon", lon);
        System.out.println(json.toString());

        try{
            connectionISC.sendPut(directory, json.toString());
        }catch (Exception e){
            System.out.println(e);
        }

    }

    // Get capability value from a uuid
    public static String GetCapabilityLastValue(String uuid, String Capability){
        // building POST directory
        String directory = "collector/resources/" + uuid + "/data/last";

        String JSONValue = "";
        try{
            JSONValue = connectionISC.sendPost(directory, "");
        }catch (Exception e){
            System.out.println(e);
            return "";
        }

        JSONObject jsonReturnPost = new JSONObject(JSONValue.toString());
        return jsonReturnPost.getJSONArray("resources").getJSONObject(0)
                .getJSONObject("capabilities").getJSONArray(Capability).getJSONObject(0).get("value").toString();

    }

    // Get capability value from a uuid
    public static String GetCapabilityLastValue(String uuid){
        // building POST directory
        String directory = "collector/resources/" + uuid + "/data/last";

        String JSONValue = "";
        try{
            JSONValue = connectionISC.sendPost(directory, "");
        }catch (Exception e){
            System.out.println(e);
            return "";
        }

        JSONObject jsonReturnPost = new JSONObject(JSONValue.toString());
        return jsonReturnPost.getJSONArray("resources").getJSONObject(0)
                .getJSONObject("capabilities").toString();

    }

    // Add a Patrol and returns its uuid
    public static String addPatrol(Float lat,  Float lon){
        // building JSON
        JSONObject json = new JSONObject();
        json.put("description", "Patrol");
        json.put("lat", lat);
        json.put("lon", lon);
        json.put("status", "active");
        json.accumulate("capabilities", "AreaID");
        json.accumulate("capabilities", "OccurrenceID");
        json.accumulate("capabilities", "Latitude");
        json.accumulate("capabilities", "Longitude");
        json.accumulate("capabilities", "Position");
        JSONObject data = new JSONObject();
        data.put("data",json);
        //System.out.println(data.toString());

        String JSONUuid = "";
        try{
            JSONUuid = connectionISC.sendPost("catalog/resources",data.toString());

        }catch (Exception e){
            System.out.println(e);
            return "";
        }

        System.out.println("--- ISC: Patrol ADDED");

        JSONObject jsonReturnPost = new JSONObject(JSONUuid.toString());
        jsonReturnPost = jsonReturnPost.getJSONObject("data");
        return jsonReturnPost.get("uuid").toString();
    }

    // Add a Patrol and returns its uuid
    public static String addResident(){
        // building JSON
        JSONObject json = new JSONObject();
        json.put("description", "Patrol");
        json.put("lat", 0f);
        json.put("lon", 0f);
        json.put("status", "active");
        json.accumulate("capabilities", "AreaID");
        json.accumulate("capabilities", "Latitude");
        json.accumulate("capabilities", "Longitude");
        JSONObject data = new JSONObject();
        data.put("data",json);
        //System.out.println(data.toString());

        String JSONUuid = "";
        try{
            JSONUuid = connectionISC.sendPost("catalog/resources",data.toString());

        }catch (Exception e){
            System.out.println(e);
            return "";
        }

        System.out.println("--- ISC: Resident ADDED");

        JSONObject jsonReturnPost = new JSONObject(JSONUuid.toString());
        jsonReturnPost = jsonReturnPost.getJSONObject("data");
        return jsonReturnPost.get("uuid").toString();
    }

    // Add a Resident and returns its uuid
    public static String addResident(Float lat,  Float lon){
        // building JSON
        JSONObject json = new JSONObject();
        json.put("description", "Resident");
        json.put("lat", lat);
        json.put("lon", lon);
        json.put("status", "active");
        json.accumulate("capabilities", "AreaID");
        json.accumulate("capabilities", "Latitude");
        json.accumulate("capabilities", "Longitude");
        JSONObject data = new JSONObject();
        data.put("data",json);
        System.out.println(data.toString());

        String JSONUuid = "";
        try{
            JSONUuid = connectionISC.sendPost("catalog/resources",data.toString());

        }catch (Exception e){
            System.out.println(e);
            return "";
        }

        JSONObject jsonReturnPost = new JSONObject(JSONUuid.toString());
        jsonReturnPost = jsonReturnPost.getJSONObject("data");
        return jsonReturnPost.get("uuid").toString();
    }

    // Add a Resident and returns its uuid
    public static String addOccurrence(){
        // building JSON
        JSONObject json = new JSONObject();
        json.put("description", "Occurrence");
        json.put("lat", 0f);
        json.put("lon", 0f);
        json.put("status", "active");
        json.accumulate("capabilities", "NotifierID");
        json.accumulate("capabilities", "Situation");
        json.accumulate("capabilities", "PatrolID");
        json.accumulate("capabilities", "Type");
        JSONObject data = new JSONObject();
        data.put("data",json);
        //System.out.println(data.toString());

        String JSONUuid = "";
        try{
            JSONUuid = connectionISC.sendPost("catalog/resources",data.toString());

        }catch (Exception e){
            System.out.println(e);
            return "";
        }

        System.out.println("--- ISC: Occurrence ADDED");

        JSONObject jsonReturnPost = new JSONObject(JSONUuid.toString());
        jsonReturnPost = jsonReturnPost.getJSONObject("data");
        return jsonReturnPost.get("uuid").toString();
    }

    // Add a Area and returns its uuid
    public static String addArea(Float lat,  Float lon){
        // building JSON
        JSONObject json = new JSONObject();
        json.put("description", "Area");
        json.put("lat", lat);
        json.put("lon", lon);
        json.put("status", "active");
        json.accumulate("capabilities", "AreaSize");
        json.accumulate("capabilities", "OccurrenceID");
        JSONObject data = new JSONObject();
        data.put("data",json);
        System.out.println(data.toString());

        String JSONUuid = "";
        try{
            JSONUuid = connectionISC.sendPost("catalog/resources",data.toString());

        }catch (Exception e){
            System.out.println(e);
            return "";
        }

        JSONObject jsonReturnPost = new JSONObject(JSONUuid.toString());
        jsonReturnPost = jsonReturnPost.getJSONObject("data");
        return jsonReturnPost.get("uuid").toString();
    }

    // Add a Occurrence and returns its uuid
    public static String addOccurrence(Float lat,  Float lon){
        // building JSON
        JSONObject json = new JSONObject();
        json.put("description", "Occurrence");
        json.put("lat", lat);
        json.put("lon", lon);
        json.put("status", "active");
        json.accumulate("capabilities", "AreaID");
        json.accumulate("capabilities", "NotifierID");
        json.accumulate("capabilities", "Situation");
        json.accumulate("capabilities", "PatrolID");
        JSONObject data = new JSONObject();
        data.put("data",json);
        System.out.println(data.toString());

        String JSONUuid = "";
        try{
            JSONUuid = connectionISC.sendPost("catalog/resources",data.toString());

        }catch (Exception e){
            System.out.println(e);
            return "";
        }

        JSONObject jsonReturnPost = new JSONObject(JSONUuid.toString());
        jsonReturnPost = jsonReturnPost.getJSONObject("data");
        return jsonReturnPost.get("uuid").toString();
    }

    // Used to add the Default Capabilities - must run before the others methods
    public static String addDefaultCapabilities(){

        List<JSONObject> JSONList = new ArrayList<JSONObject>();

        /*
        // building JSON
        JSONObject AreaID = new JSONObject();
        AreaID.put("name", "AreaID");
        AreaID.put("description", "ID of the Area");
        AreaID.put("capability_type", "sensor");
        JSONList.add(AreaID);

        JSONObject OccurrenceID = new JSONObject();
        OccurrenceID.put("name", "OccurrenceID");
        OccurrenceID.put("description", "ID of the Occurrence");
        OccurrenceID.put("capability_type", "sensor");
        JSONList.add(OccurrenceID);

        JSONObject AreaSize = new JSONObject();
        AreaSize.put("name", "AreaSize");
        AreaSize.put("description", "Size of the area");
        AreaSize.put("capability_type", "sensor");
        JSONList.add(AreaSize);

        JSONObject NotifierID = new JSONObject();
        NotifierID.put("name", "NotifierID");
        NotifierID.put("description", "ID of the Notifier");
        NotifierID.put("capability_type", "sensor");
        JSONList.add(NotifierID);

        JSONObject Situation = new JSONObject();
        Situation.put("name", "Situation");
        Situation.put("description", "Status of the situation");
        Situation.put("capability_type", "sensor");
        JSONList.add(Situation);

        JSONObject PatrolID = new JSONObject();
        PatrolID.put("name", "PatrolID");
        PatrolID.put("description", "ID of the Patrol");
        PatrolID.put("capability_type", "sensor");
        JSONList.add(PatrolID);

        JSONObject Latitude = new JSONObject();
        Latitude.put("name", "Latitude");
        Latitude.put("description", "Latitude on real time");
        Latitude.put("capability_type", "sensor");
        JSONList.add(Latitude);

        JSONObject Longitude = new JSONObject();
        Longitude.put("name", "Longitude");
        Longitude.put("description", "Longitude on real time");
        Longitude.put("capability_type", "sensor");
        JSONList.add(Longitude);

        JSONObject Position = new JSONObject();
        Position.put("name", "Position");
        Position.put("description", "Indicate the Patrol Position");
        Position.put("capability_type", "sensor");
        JSONList.add(Position);

        JSONObject Type = new JSONObject();
        Type.put("name", "Type");
        Type.put("description", "Type of Occurrence (1,2,3) indicates the maximum number of Patrols");
        Type.put("capability_type", "sensor");
        JSONList.add(Type);*/

        String result= "";
        try{
            for(JSONObject JSONobj : JSONList){
                result += connectionISC.sendPost("catalog/capabilities", JSONobj.toString());
            }
        }catch (Exception e){
            System.out.println(e);
            return result;
        }

        return result;
    }

}
