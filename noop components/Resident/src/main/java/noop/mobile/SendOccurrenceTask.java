package noop.mobile;

import camilawanous.noopdata.Location;

import java.util.concurrent.ThreadLocalRandom;

public class SendOccurrenceTask implements Runnable{

    private Resident resident;

    public SendOccurrenceTask(Resident resident){
        this.resident = resident;
    }

    @Override
    public void run() {

        Integer SendOrNot = ThreadLocalRandom.current().nextInt(4); // 10%
        SendOrNot =1;
        if(SendOrNot == 1 && !this.resident.OccurenceNotify){

            this.resident.OccurenceNotify = true;

            //Integer OccurrenceType = ThreadLocalRandom.current().nextInt(3) + 1; // 0, 1, 2
            Integer OccurrenceType = this.resident.OccurrenceTypeDefault;

            Location OccurrenceLocation = this.resident.location;

            this.resident.SendNoopJASONDataToMonitor(this.resident.CreateOccurrence(OccurrenceType, OccurrenceLocation));

            System.out.println("### Send Occurrence Location " + this.resident.location.toString());

            }

        try{
            //Thread.sleep(10000);
        }catch (Exception e){
            e.printStackTrace();
        }

        return;

    }
}
