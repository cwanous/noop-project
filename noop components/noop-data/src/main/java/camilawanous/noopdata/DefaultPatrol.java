package camilawanous.noopdata;

import java.util.UUID;

public class DefaultPatrol {

    public UUID patrolId;
    public String area;
    public String position;
    public Location location;

    public DefaultPatrol(String Area, String Position){
        this.area = Area;
        this.position = Position;
    }

    public DefaultPatrol(String Area, UUID PatrolId){
        this.area = Area;
        this.patrolId = PatrolId;
    }

    public DefaultPatrol(){
        this.area = "-1";
    }

}
