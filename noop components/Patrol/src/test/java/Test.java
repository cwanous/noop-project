import camilawanous.noopdata.*;
import lac.cnclib.sddl.serialization.Serialization;
import noop.mobile.Patrol;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

public class Test {

    @org.junit.Test
    public void OccurrenceLocationTest() throws IOException {

        Location OccurrenceLocation = new Location(3f,3f);

        NoopJASONData JSONOccurrence = new NoopJASONData(DataType.OCCURRENCE);
        JSONOccurrence.add("Type","1");
        JSONOccurrence.add("Location",OccurrenceLocation.toString());

        NoopJASONData JSONData = new NoopJASONData(Serialization.fromJavaByteStream(Serialization.toJavaByteStream(JSONOccurrence.toString())).toString());

        System.out.println(JSONData.getByKey("Location"));

        Location locationReply = new Location(JSONData.getByKey("Location"));

    }


    @org.junit.Test
    public void TestCityArea(){

        CityArea ca = new CityArea(4);
        Area ar1 = ca.mapOfAreas.get("6");
        Area ar2 = ca.mapOfAreas.get("11");
        System.out.println(ar1.toString());
        System.out.println(ar2.toString());
        System.out.println(ca.distance(ar1,ar2).toString());
        System.out.println(ca.distance("6","11").toString());

        UUID id1 = UUID.randomUUID();
        UUID id2 = UUID.randomUUID();
        UUID id3 = UUID.randomUUID();
        DefaultPatrol d1 = new DefaultPatrol("13", "ToArea" );
        DefaultPatrol d2 = new DefaultPatrol("8", "ToArea" );
        DefaultPatrol d3 = new DefaultPatrol("12", "OnArea" );
        Map< UUID, DefaultPatrol> map = new HashMap<>();
        //map.put(id1, d1);
        //map.put(id2, d2);
        //map.put(id3, d3);

        System.out.println(id1.toString());
        System.out.println(id2.toString());
        System.out.println(id3.toString());

        System.out.println(ca.CalculateNewPatrolArea(map));

        DefaultPatrol d4 = ca.PatrolOptimization(map);
        System.out.println("Area: " + d4.area + " - UUID: " + d4.patrolId);

        Location locTest = new Location(2f,2f);
        Area areaTest = ca.GetAreaFromLocation(locTest);
        System.out.println("Area Test: " + areaTest.toString());
    }


    @org.junit.Test
    public void TestPatrolArea(){

        Patrol P = new Patrol("DEBUG");

        P.patrolMove.NewPatrolMoveTask("1");

        try {
            Thread.sleep(600000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }


    @org.junit.Test
    public void TestRandom(){

        for(int j = 0; j<10 ; j++){
            Integer i = ThreadLocalRandom.current().nextInt(2);
            System.out.println(i.toString());
        }

    }

}
