package camilawanous.noopdata;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * A Class that format the data exchanged between Core and Mobile Nodes
 *
 * {type = , [must have this field only]
 * field1 = ,
 * field2 = ,
 *      }
 *
 * @author Camila Wanous
 */


public class NoopJASONData implements Serializable {

    private JSONObject JASONData;

    public NoopJASONData(DataType type){
        JASONData = new JSONObject();
        JASONData.put("type", type.toString());
    }

    public NoopJASONData(String JSONString){
        JASONData = new JSONObject(JSONString);
    }

    public String getType(){
        return this.JASONData.get("type").toString();
    }

    public void add(String key, String value){
        this.JASONData.put(key,value);
    }

    public String getByKey(String key){
        try{
            return this.JASONData.get(key).toString();
        }catch (Exception e){
            return "NO KEY";
        }
    }

    @Override
    public String toString() {
        return JASONData.toString();
    }

}
