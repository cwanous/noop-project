package camilawanous.interscity.isctasks;

import camilawanous.interscity.Monitor;

public class AddPatrolTask implements Runnable{

    private Monitor monitor;
    private Float lat;
    private Float lon;

    public AddPatrolTask(Monitor Monitor, Float Lat, Float Lon){
        this.monitor = Monitor;
        this.lat = Lat;
        this.lon = Lon;

    }

    @Override
    public void run() {
        this.monitor.monitordata.addPatrol(this.lat, this.lon);
        return;
    }
}
