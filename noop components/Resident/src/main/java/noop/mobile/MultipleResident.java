package noop.mobile;

import java.util.ArrayList;
import java.util.List;

public class MultipleResident {

    public static void main(String[] args) {

        new MultipleResident(args[0]);

    }


    public MultipleResident(String Test){

        String[] occurrenceAreaList = new String[8];
        String[] occurrenceTypeList = new String[8];

        occurrenceTypeList = new String[]{"1", "1", "1", "1", "1", "1", "1", "1"};

        // 4
        switch (Test){
            case "1":{
                occurrenceAreaList = new String[]{"14", "13", "13", "9", "3", "7", "2", "4"};
                //occurrenceTypeList = new String[]{"1", "1", "1", "1", "1", "1", "1", "1"};
                break;
            }
            case "2":{
                occurrenceAreaList = new String[]{"7", "12", "14", "2", "9", "7", "12", "9"};
                //occurrenceTypeList = new String[]{"2", "2", "2", "3", "3", "1", "1", "1"};
                break;
            }
            case "3":{
                occurrenceAreaList = new String[]{"5", "16", "15", "11", "7", "5", "7", "11"};
                //occurrenceTypeList = new String[]{"1", "2", "3", "1", "2", "1", "3", "3"};
                break;
            }
            case "4":{
                occurrenceAreaList = new String[]{"5", "10", "9", "16", "1", "1", "2", "9"};
                //occurrenceTypeList = new String[]{"1", "3", "1", "2", "3", "3", "2", "3"};
                break;
            }
            case "5":{
                occurrenceAreaList = new String[]{"14", "7", "14", "2", "4", "13", "3", "16"};
                //occurrenceTypeList = new String[]{"3", "2", "2", "2", "1", "1", "3", "3"};
                break;
            }
            case "6":{
                occurrenceAreaList = new String[]{"6", "15", "3", "14", "2", "1", "12", "11"};
                //occurrenceTypeList = new String[]{"3", "1", "3", "2", "3", "2", "1", "1"};
                break;
            }
            case "7":{
                occurrenceAreaList = new String[]{"10", "4", "4", "15", "9", "12", "15", "14"};
                //occurrenceTypeList = new String[]{"3", "1", "1", "2", "3", "1", "1", "1"};
                break;
            }
            case "8":{
                occurrenceAreaList = new String[]{"14", "15", "14", "11", "15", "14", "13", "13"};
                //occurrenceTypeList = new String[]{"1", "2", "1", "3", "1", "2", "3", "2"};
                break;
            }
            case "9":{
                occurrenceAreaList = new String[]{"10", "14", "12", "6", "9", "2", "3", "13"};
                //occurrenceTypeList = new String[]{"2", "2", "3", "2", "3", "1", "2", "2"};
                break;
            }
            case "10":{
                occurrenceAreaList = new String[]{"6", "1", "7", "14", "7", "9", "5", "1"};
                //occurrenceTypeList = new String[]{"3", "2", "3", "3", "3", "2", "3", "3"};
                break;
            }
            case "11":{
                occurrenceAreaList = new String[]{"14", "1", "12", "3", "3", "5", "3", "3"};
                //occurrenceTypeList = new String[]{"3", "3", "3", "3", "2", "1", "1", "1"};
                break;
            }
            case "12":{
                occurrenceAreaList = new String[]{"1", "7", "14", "8", "14", "5", "6", "9"};
                //occurrenceTypeList = new String[]{"2", "2", "2", "2", "1", "3", "1", "3"};
                break;
            }
            case "13":{
                occurrenceAreaList = new String[]{"8", "12", "16", "15", "14", "11", "16", "11"};
                //occurrenceTypeList = new String[]{"3", "2", "2", "3", "2", "2", "3", "1"};
                break;
            }
            case "14":{
                occurrenceAreaList = new String[]{"12", "11", "16", "11", "8", "9", "9", "12"};
                //occurrenceTypeList = new String[]{"1", "2", "2", "1", "2", "1", "3", "3"};
                break;
            }
            case "15":{
                occurrenceAreaList = new String[]{"13", "7", "3", "5", "6", "15", "13", "15"};
                //occurrenceTypeList = new String[]{"1", "1", "1", "1", "2", "1", "2", "1"};
                break;
            }
        }

        // 5
        /*switch (Test){
            case "1":{
                occurrenceAreaList = new String[]{"3", "22", "2", "2", "9", "17", "12", "17"};
                //occurrenceTypeList = new String[]{"2", "3", "1", "2", "1", "2", "1", "2"};
                occurrenceTypeList = new String[]{"1", "1", "1", "1", "1", "1", "1", "1"};
                break;
            }
            case "2":{
                occurrenceAreaList = new String[]{"7", "23", "19", "9", "14", "2", "2", "23"};
                occurrenceTypeList = new String[]{"1", "3", "3", "2", "2", "2", "3", "2"};
                break;
            }
            case "3":{
                occurrenceAreaList = new String[]{"14", "14", "10", "7", "1", "21", "1", "2"};
                occurrenceTypeList = new String[]{"1", "1", "2", "2", "3", "3", "1", "2"};
                break;
            }
            case "4":{
                occurrenceAreaList = new String[]{"13", "20", "14", "1", "16", "11", "6", "2"};
                occurrenceTypeList = new String[]{"1", "2", "3", "2", "2", "3", "1", "2"};
                break;
            }
            case "5":{
                occurrenceAreaList = new String[]{"11", "1", "11", "24", "7", "1", "7", "20"};
                occurrenceTypeList = new String[]{"3", "3", "2", "2", "1", "1", "1", "3"};
                break;
            }
            case "6":{
                occurrenceAreaList = new String[]{"16", "22", "4", "8", "16", "4", "9", "3"};
                occurrenceTypeList = new String[]{"3", "1", "3", "1", "1", "2", "3", "2"};
                break;
            }
            case "7":{
                occurrenceAreaList = new String[]{"5", "18", "20", "16", "18", "2", "14", "8"};
                occurrenceTypeList = new String[]{"1", "2", "2", "1", "2", "2", "2", "3"};
                break;
            }
            case "8":{
                occurrenceAreaList = new String[]{"21", "18", "2", "3", "22", "10", "13", "20"};
                occurrenceTypeList = new String[]{"3", "3", "3", "1", "1", "3", "3", "3"};
                break;
            }
            case "9":{
                occurrenceAreaList = new String[]{"22", "13", "7", "11", "8", "20", "13", "13"};
                occurrenceTypeList = new String[]{"1", "3", "3", "3", "1", "2", "3", "3"};
                break;
            }
            case "10":{
                occurrenceAreaList = new String[]{"10", "21", "3", "9", "15", "9", "17", "20"};
                occurrenceTypeList = new String[]{"1", "2", "2", "1", "1", "1", "2", "1"};
                break;
            }
            case "11":{
                occurrenceAreaList = new String[]{"17", "6", "6", "5", "15", "20", "6", "11"};
                occurrenceTypeList = new String[]{"3", "2", "2", "2", "2", "2", "3", "1"};
                break;
            }
            case "12":{
                occurrenceAreaList = new String[]{"10", "1", "13", "13", "23", "6", "5", "6"};
                occurrenceTypeList = new String[]{"3", "2", "2", "2", "2", "2", "3", "1"};
                break;
            }
            case "13":{
                occurrenceAreaList = new String[]{"24", "4", "6", "10", "14", "6", "13", "1"};
                occurrenceTypeList = new String[]{"1", "3", "2", "3", "3", "3", "2", "2"};
                break;
            }
            case "14":{
                occurrenceAreaList = new String[]{"11", "9", "15", "12", "17", "1", "21", "6"};
                occurrenceTypeList = new String[]{"1", "2", "3", "1", "3", "1", "2", "1"};
                break;
            }
            case "15":{
                occurrenceAreaList = new String[]{"24", "20", "9", "21", "24", "19", "21", "22"};
                occurrenceTypeList = new String[]{"3", "2", "3", "1", "2", "2", "1", "2"};
                break;
            }
        }*/


        List<Resident> listResident = new ArrayList<Resident>();

        for(int i =0; i<8; i++){

            try {
                Resident resident = new Resident(occurrenceAreaList[i], occurrenceTypeList[i]);
                listResident.add(resident);
                Thread.sleep(60);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        synchronized (this) {
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


}
