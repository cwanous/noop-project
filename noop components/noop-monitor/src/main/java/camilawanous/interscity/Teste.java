package camilawanous.interscity;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Teste {

    private static final String USER_AGENT = "Mozilla/5.0";
    private static String interSCityURI;


    public static void main(String[] args) throws Exception {
        interSCityURI = "http://172.17.0.1:8000";
        String catalog = sendGet("catalog/capabilities","");
        System.out.println(catalog);
    }


    public static  String sendDelete(String directory, String data) throws Exception {
        final String url = interSCityURI + "/" + directory + "/" + data;
        final String method = "DELETE";
        HttpURLConnection con = (HttpURLConnection) (new URL(url)).openConnection();
        con.setRequestMethod(method);
        con.setRequestProperty("User-agent", USER_AGENT);
        con.setDoOutput(true);
        int responseCode = con.getResponseCode();
        if(responseCode /100 != 2) { throw new Exception(); } BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream())); String inputLine;
        StringBuffer answer = new StringBuffer();
        while((inputLine = in.readLine()) != null) {
            answer.append(inputLine) ;
        }
        in.close();
        con.disconnect();
        return answer.toString();
    }


    public static String sendGet(String directory, String data) throws Exception {
        final String url = interSCityURI + "/" + directory + "?" + data;
        final String method = "GET";
        System.out.println(url);
        HttpURLConnection con = (HttpURLConnection) (new URL(url)).openConnection();
        con.setRequestMethod(method);
        con.setRequestProperty("User-agent", USER_AGENT);
        int responseCode;
        try {
            responseCode = con.getResponseCode();
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
        if(responseCode /100 != 2) { throw new Exception(); }
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer answer = new StringBuffer();
        while ((inputLine = in.readLine()) != null) { answer.append(inputLine) ; }
        in.close();
        con.disconnect();
        return answer.toString();
    }


    public static String sendPut(String directory, String data) throws Exception {
        final String url = interSCityURI + "/" + directory;
        final String method = "PUT";
        HttpURLConnection con = (HttpURLConnection) (new URL(url)).openConnection();
        con.setRequestMethod(method);
        con.setRequestProperty("User-agent", USER_AGENT);
        con.setRequestProperty("Content-Type", "application/json");
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(data);
        wr.flush();
        wr.close();
        int responseCode = con.getResponseCode();
        if(responseCode /100 != 2) { throw new Exception(); }
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer answer = new StringBuffer();
        while ((inputLine = in.readLine()) != null) { answer.append(inputLine) ; }
        in.close();
        con.disconnect();
        return answer.toString();
    }


    public static String sendPost(String directory, String jsonString) throws Exception {
        final String url = interSCityURI+"/"+directory;  final String method = "POST";  String inputLine;
        HttpURLConnection con = (HttpURLConnection) (new URL(url)).openConnection();
        con.setRequestMethod(method);
        con.setRequestProperty("User-agent", USER_AGENT);
        con.setRequestProperty("Content-Type", "application/json");
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(jsonString);   wr.flush();   wr.close();
        int responseCode = con.getResponseCode();
        if(responseCode /100 != 2) {
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getErrorStream()));
            StringBuffer answer = new StringBuffer();
            while ((inputLine = in.readLine()) != null) { answer.append(inputLine) ; }
            in.close();   con.disconnect();
            throw new Exception("Code: " + responseCode + ": " + answer.toString());
        }
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        StringBuffer answer = new StringBuffer();
        while ((inputLine = in.readLine()) != null) { answer.append(inputLine) ; }
        in.close();   con.disconnect();
        return answer.toString();
    }
}
