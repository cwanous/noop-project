package noop.mobile;

import camilawanous.noopdata.*;
import lac.cnclib.net.NodeConnection;
import lac.cnclib.net.NodeConnectionListener;
import lac.cnclib.net.groups.Group;
import lac.cnclib.net.groups.GroupCommunicationManager;
import lac.cnclib.net.groups.GroupMembershipListener;
import lac.cnclib.net.mrudp.MrUdpNodeConnection;
import lac.cnclib.sddl.message.ApplicationMessage;
import lac.cnclib.sddl.message.Message;
import lac.cnclib.sddl.serialization.Serialization;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Patrol implements NodeConnectionListener, GroupMembershipListener {

    // Variables to locate the gateway, its IP address and port number
    private static String gatewayIP    = "127.0.0.1";
    private static int gatewayPort  = 5500;
    private MrUdpNodeConnection	connection;

    // Variables to GroupMembershipListener trigger events
    private GroupCommunicationManager groupManager;
    private Group aGroup;

    // Thread Poll
    private final ScheduledThreadPoolExecutor threadPool;
    private static final int INTERVAL_IN_SECS = 10;

    // Location
    public PatrolMove patrolMove; // only initiated after Connection
    public String patrolArea = "";

    // CityArea
    private CityArea cityArea;

    // ID
    private final UUID patrolId;

    // Position
    private PatrolPosition position;

    // Occurrence
    private String occurrenceIndex = "";
    private String occurrenceSenderIndex = "";

    // The constructor make the connection with the gateway
    public Patrol() {

        int maximumPoolSize  = Runtime.getRuntime().availableProcessors() * 2;

        InetSocketAddress address = new InetSocketAddress(gatewayIP, gatewayPort);

        this.patrolId = UUID.randomUUID();
        this.position = PatrolPosition.RequestArea;

        // Starting City Area
        this.cityArea = new CityArea(4);

        // Starting location
        this.patrolMove = new PatrolMove(this);

        // Starting threadPool that handles Send Location task and Send Occurrence Task

        this.threadPool = new ScheduledThreadPoolExecutor(maximumPoolSize);
        this.threadPool.prestartAllCoreThreads();
        final SendLocationTask sendlocationtask = new SendLocationTask(this);
        final SendOccurrenceTask sendOccurrenceTask = new SendOccurrenceTask(this);
        this.threadPool.scheduleWithFixedDelay(sendlocationtask, 2000, 2000, TimeUnit.MILLISECONDS);
        //this.threadPool.scheduleWithFixedDelay(sendOccurrenceTask, 2*INTERVAL_IN_SECS, INTERVAL_IN_SECS, TimeUnit.SECONDS);

        // Connection MR-UDP
        try {
            // Create a connection object tha encapsulate the details and protocol involved in the physical connection with GW
            connection = new MrUdpNodeConnection();
            // Add a listener to the connection object which provides methods to react to the node GW connection
            connection.addNodeConnectionListener(this);
            // Establish the connection
            connection.connect(address);

        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    public Patrol(String DEBUG) {

        this.patrolId = UUID.randomUUID();
        this.position = PatrolPosition.RequestArea;

        // Starting City Area
        this.cityArea = new CityArea(4);

        // Starting location

        // Starting threadPool that handles Send and Get Location task
        this.threadPool = new ScheduledThreadPoolExecutor(100);
        this.threadPool.prestartAllCoreThreads();

        // Connection MRUDP

        // Starts Moving
        this.patrolMove = new PatrolMove(this);

    }


    public static void main(String[] args) {

        Logger.getLogger("").setLevel(Level.OFF);

        new Patrol();
    }

    // Send Message to the Monitor
    public void SendMessageToMonitor(ApplicationMessage message){

        try {
            connection.sendMessage(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //System.out.println("Send Msg");
    }

    // Send Message to the Group Definer
    public void SendMessageToGD(){

    }

    // Creates an ApplicationMessage from a JSONData
    public ApplicationMessage CreateApplicationMessage(NoopJASONData JSONData) throws IOException {
        // Creating message
        ApplicationMessage message = new ApplicationMessage();
        message.setContentObject(JSONData.toString());
        try {
            message.setContent(Serialization.toJavaByteStream(JSONData.toString()));
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
        return message;
    }

    // Once connected the Patrol, stars moving and requires an AreaID and sent status
    @Override
    public void connected(NodeConnection remoteCon) {
        System.out.println("# Connected");

        // Starts Group Listener
        this.groupManager = new GroupCommunicationManager(remoteCon);
        this.groupManager.addMembershipListener(this);

        // Message content
        NoopJASONData JSONRequestArea = new NoopJASONData(DataType.AREA);
        JSONRequestArea.add("ID",this.patrolId.toString());
        System.out.println("### REQUEST AREA");

        SendNoopJASONDataToMonitor(JSONRequestArea);

    }

    @Override
    public void newMessageReceived(NodeConnection remoteCon, Message message) {
        //System.out.println("##### Message Received");

        // Get Message Content
        NoopJASONData JSONData = new NoopJASONData(Serialization.fromJavaByteStream(message.getContent()).toString());

        if(this.position == PatrolPosition.RequestArea ||
                this.position == PatrolPosition.ToArea ||
                this.position == PatrolPosition.OnArea){
            //this.position = PatrolPosition.Hold;
            switch (JSONData.getType().toString()){
                case "AREA": {
                    String newArea = JSONData.getByKey("AreaID");
                    if (!newArea.equals(this.patrolArea)){
                        System.out.println("### AREA Message Received");
                        this.position = PatrolPosition.ToArea;
                        ProcessAreaMessage(JSONData);
                    }else{
                        Arrives();
                    }
                    break;
                }
                case "OCCURRENCE": {
                    System.out.println("### OCCURRENCE Message Received");
                    this.position = PatrolPosition.ToOccurrence;
                    ProcessOccurrenceMessage(JSONData);
                    break;
                }
                default:{
                    break;
                }
            }
        }else {
            switch (JSONData.getType().toString()) {
                case "RETURN": {
                    System.out.println("### RETURN Message Received");
                    this.position = PatrolPosition.ToArea;
                    ProcessReturnMessage(JSONData);
                    break;
                }
                default: {
                    break;
                }
            }
        }

    }


    private void ProcessOccurrenceMessage(NoopJASONData jsonData) {

        //StopPatrolMove();

        // Update Occurrence Index
        this.occurrenceIndex = jsonData.getByKey("ID");
        this.occurrenceSenderIndex = jsonData.getByKey("IDResident");

        // Get and Update Patrol Area
        this.patrolArea = jsonData.getByKey("AreaID");

        // Go to Occurrence
        this.patrolMove.NewPatrolMoveTask(this.patrolArea);

        // Sent reply to Monitor
        System.out.println("### Reply to Occurrence");
        SendPositionToMonitor();

    }


    private void ProcessAreaMessage(NoopJASONData JSONData){

        String newArea = JSONData.getByKey("AreaID");

        //StopPatrolMove();

        // Send New Position to Monitor
        SendPositionToMonitor();

        // Get and Update Patrol Area
        this.patrolArea = newArea;

        // Get Area Location (from the message sent by the Monitor)
        Location PatrolAreaLocation = new Location(JSONData.getByKey("Location"));
        System.out.println("# New Area - Location: " + PatrolAreaLocation.toString() + " Area: " + this.patrolArea);

        // Go to AreaID
        this.patrolMove.NewPatrolMoveTask(this.patrolArea);

    }


    private void ProcessReturnMessage(NoopJASONData JSONData){
        // Creates and Sends a RequestArea Message
        NoopJASONData JSONRequestArea = new NoopJASONData(DataType.AREA);
        JSONRequestArea.add("ID",this.patrolId.toString());

        SendNoopJASONDataToMonitor(JSONRequestArea);
        System.out.println("# RETURN from Occurrence");
    }


    private void StopPatrolMove(){
        System.out.println("# Patrol STOP move");
        try {
            this.patrolMove.Stop();
        }catch (Exception e){

        }
    }


    public void Arrives(){

        if(this.position == PatrolPosition.ToArea || this.position == PatrolPosition.RequestArea){
            this.position = PatrolPosition.OnArea;
            System.out.println("# Arrived ON Area");
            SendPositionToMonitor();
        }

        if(this.position == PatrolPosition.ToOccurrence){
            this.position = PatrolPosition.OnOccurrence;
            System.out.println("# Arrived ON Occurrence");
            SendPositionToMonitor();
            this.threadPool.execute(new SolveOccurrenceTask(this));
        }


    }


    public void SendPositionToMonitor(){
        NoopJASONData JSONPosition = new NoopJASONData(DataType.POSITION);
        JSONPosition.add("ID",patrolId.toString());
        JSONPosition.add("Position", this.position.toString());
        JSONPosition.add("OccurrenceID", this.occurrenceIndex);
        JSONPosition.add("IDResident", this.occurrenceSenderIndex);
        SendNoopJASONDataToMonitor(JSONPosition);
    }


    public void SendNoopJASONDataToMonitor(NoopJASONData JSONData){
        try{
            // Creating message
            ApplicationMessage message = this.CreateApplicationMessage(JSONData);
            // Sending Message
            this.SendMessageToMonitor(message);
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public NoopJASONData CreateOccurrence(Integer Type, Location Location){
        NoopJASONData JSONOccurrence = new NoopJASONData(DataType.OCCURRENCE);
        JSONOccurrence.add("ID","");
        JSONOccurrence.add("Type",Type.toString());
        JSONOccurrence.add("Location",Location.toString());
        JSONOccurrence.add("Sit","Unsolved");

        return JSONOccurrence;
    }

    @Override
    public void reconnected(NodeConnection remoteCon, SocketAddress endPoint, boolean wasHandover, boolean wasMandatory) {

    }

    @Override
    public void disconnected(NodeConnection remoteCon) {

    }

    @Override
    public void unsentMessages(NodeConnection remoteCon, List<Message> unsentMessages) {

    }

    @Override
    public void internalException(NodeConnection remoteCon, Exception e) {

    }

    @Override
    public void enteringGroups(List<Group> groups) {
    }

    @Override
    public void leavingGroups(List<Group> groups) {

    }


    public CityArea GetCityArea(){
        return this.cityArea;
    }


    public void OccurrenceSolved() {

        // Send Occurrence Solution
        NoopJASONData JSONOccurrence = new NoopJASONData(DataType.OCCURRENCE);
        JSONOccurrence.add("OccurrenceID", this.occurrenceIndex);
        JSONOccurrence.add("IDResident", this.occurrenceSenderIndex);
        JSONOccurrence.add("Sit","Solved");

        System.out.println("# Occurrence Solved");
        SendNoopJASONDataToMonitor(JSONOccurrence);

        // Update Patrol Position
        this.position = PatrolPosition.RequestArea;

        // Request Area
        NoopJASONData JSONRequestArea = new NoopJASONData(DataType.AREA);
        JSONRequestArea.add("ID",this.patrolId.toString());

        SendNoopJASONDataToMonitor(JSONRequestArea);

        this.occurrenceIndex = "";
        this.occurrenceSenderIndex = "";

    }


    public PatrolPosition GetPatrolPosition(){
        return this.position;
    }


    public UUID GetPatrolId(){
        return this.patrolId;
    }
}
