package noop.core;

import camilawanous.noopdata.NoopJASONData;
import lac.cnclib.sddl.serialization.Serialization;
import lac.cnet.sddl.objects.ApplicationObject;
import lac.cnet.sddl.objects.Message;
import lac.cnet.sddl.udi.core.SddlLayer;
import lac.cnet.sddl.udi.core.UniversalDDSLayerFactory;
import lac.cnet.sddl.udi.core.listener.UDIDataReaderListener;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class MonitorLog implements UDIDataReaderListener<ApplicationObject> {

    /**
     * ContextNet
     */
    SddlLayer core;

    /**
     *  Message Register Map
     */
    public static Map<NoopJASONData, Instant> LogRegisterMap;

    public static void main(String[] args) {

        Runtime.getRuntime().addShutdownHook(new Thread( () -> {
            try {
                close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }));
        new MonitorLog();
    }

    public MonitorLog(){

        // Log Register Map
        LogRegisterMap = new ConcurrentHashMap<>();

        // ContextNet
        core = UniversalDDSLayerFactory.getInstance();
        core.createParticipant(UniversalDDSLayerFactory.CNET_DOMAIN);
        core.createSubscriber();

        // Data reader for the Message topic (application messages)
        Object receiveMessageTopic = core.createTopic(Message.class, Message.class.getSimpleName());
        core.createDataReader(this, receiveMessageTopic);

        System.out.println("===== LOG Started =====");

        synchronized (this) {
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onNewData(ApplicationObject applicationObject) {

        System.out.println("### New Message ###");

        Message message = (Message) applicationObject;

        NoopJASONData JSONData = new NoopJASONData(Serialization.fromJavaByteStream(message.getContent()).toString());

        Instant now = Instant.now();

        LogRegisterMap.put(JSONData, now);

    }

    public static void close() throws IOException {
        System.out.println("### Close ###");
        List<String> lines = Arrays.asList(LogRegisterMap.toString());
        Path file = Paths.get("the-file-name.txt");
        Files.write(file, lines, StandardCharsets.UTF_8);
        //System.out.println(LogRegisterMap.toString());
    }
}
