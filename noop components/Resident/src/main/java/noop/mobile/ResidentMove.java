package noop.mobile;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ResidentMove {

    private static final int maximumPoolSize = Runtime.getRuntime().availableProcessors() * 2;
    private Resident resident;
    private final ExecutorService threadPool;

    public ResidentMove(Resident Resident){
        this.resident = Resident;
        this.threadPool = Executors.newFixedThreadPool(maximumPoolSize);
    }


}
