package camilawanous.noopdata;

import org.json.JSONObject;

/**
 * A Class that represents the Location
 *
 *
 * @author Camila Wanous
 */

public class Location {

    private Float Latitude;
    private Float Longitude;

    public Location(){
        Latitude = 0f;
        Longitude = 0f;
    }

    public Location(Float Lat, Float Lon){
        Latitude = Lat;
        Longitude = Lon;
    }

    public Location(String location){
        JSONObject str = new JSONObject(location);
        this.Latitude = str.getFloat("LA");
        this.Longitude = str.getFloat("LO");
    }

    public Float getLatitude(){
        return Latitude;
    }

    public Float getLongitude(){
        return Longitude;
    }

    public void setLatitude(Float Lat){
        Latitude = Lat;
    }

    public void setLongitude(Float Lon){
        Longitude = Lon;
    }

    public void IncreaseLatitude(Float Lat){
        Latitude = Latitude + Lat;
    }

    public void IncreaseLongitude(Float Lon){
        Longitude = Longitude + Lon;
    }

    @Override
    public String toString(){
        JSONObject str = new JSONObject();
        str.put("LA", this.Latitude);
        str.put("LO", this.Longitude);
        return  str.toString();
    }
}
