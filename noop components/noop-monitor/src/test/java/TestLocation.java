import camilawanous.noopdata.DataType;
import camilawanous.noopdata.Location;
import camilawanous.noopdata.NoopJASONData;
import org.junit.Test;

public class TestLocation {

    @Test
    public void Test(){
        // Get Area Location on InterSCity
        Location PoliceAreaLocation = new Location(10.5f,10.5f);

        // Build and Send New Message
        NoopJASONData JSONLocation = new NoopJASONData(DataType.AREA);
        JSONLocation.add("Location",PoliceAreaLocation.toString());

        Location PatrolAreaLocation = new Location(JSONLocation.getByKey("Location"));
        System.out.println("Area Location: " + PatrolAreaLocation.toString());
    }
}
