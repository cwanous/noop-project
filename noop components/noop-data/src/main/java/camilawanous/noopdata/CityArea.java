package camilawanous.noopdata;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadLocalRandom;

public class CityArea {

    public Map<String, Area> mapOfAreas;
    // number of areas per side
    private Integer areasPerSide;
    private Integer sizeOfArea;
    private Integer numOfAreas;

    Float maxX;
    Float maxY;


    public Integer GetNumOfAreas(){
        return this.numOfAreas;
    }


    public Integer GetAreasPerSide(){
        return this.areasPerSide;
    }


    public CityArea(Integer AreasPerSide){

        this.areasPerSide = AreasPerSide;
        this.numOfAreas = areasPerSide*areasPerSide;

        this.mapOfAreas = new ConcurrentHashMap<String, Area>();


        Integer count = 1;
        for (Integer y = 1; y<=areasPerSide; y++){
            for (Integer x = 1; x<=areasPerSide; x++){
                Integer xMin = (x-1); Integer xMax = x;
                Integer yMin = (y-1); Integer yMax = y;

                Area area =new Area(count.toString(), xMin, xMax, yMin, yMax);
                mapOfAreas.put(count.toString(),area);
                count++;

                if(y == areasPerSide && x == areasPerSide){
                    this.maxX = (float) xMax;
                    this.maxY = (float) yMax;
                }

            }
        }


    }

    // RETURN
    public DefaultPatrol PatrolOptimization(Map<UUID, DefaultPatrol> map){

        DefaultPatrol newPatrolOptimization = new DefaultPatrol();


        if(map != null) {
            if (map.size() > 0) {
                Map<UUID, String> listOfAreasPatrolled = GetListOfAreasPatrolled(map);

                Optimization PossibleOptimization = GetPossibleOptimization(listOfAreasPatrolled);

                System.out.println("### OPT ### Add:" + PossibleOptimization.areaAdded.toString() +
                        " - Removed:" + PossibleOptimization.areaRemoved.toString());
                if(PossibleOptimization.areaAdded != "" && PossibleOptimization.areaRemoved != ""){

                    for (Map.Entry<UUID, String> entry : listOfAreasPatrolled.entrySet()){

                        if(entry.getValue() == PossibleOptimization.areaRemoved){
                            newPatrolOptimization.patrolId = entry.getKey();
                        }
                    }

                    newPatrolOptimization.area = PossibleOptimization.areaAdded;
                    newPatrolOptimization.location = GetLocationFromArea(newPatrolOptimization.area);
                    return newPatrolOptimization;

                }else{
                    return newPatrolOptimization;
                }
            }
        }
        return newPatrolOptimization;
    }


    // Get Possible Optimization

    // Changed 2: NOOP3
    /*public Optimization GetPossibleOptimization(Map<UUID, String> listOfAreasPatrolled){

        // Get New Possible Area
        String newArea = "1";
        String removedArea = "1";
        Integer maxNumberOfAreasPatrolled = 0;

        Set<Integer> SetOfAreasPatrolledNow = new HashSet<>();

        for(Map.Entry<UUID, String> entry : listOfAreasPatrolled.entrySet()){
            SetOfAreasPatrolledNow.addAll(GetNumberOfAreasPatrolled(entry.getValue()));
        }

        // Iterate Areas to find the best
        for (Integer i = 1; i<=numOfAreas; i++){
            if(!listOfAreasPatrolled.containsValue(i.toString())){
                Set<Integer> SetOfAreasPatrolledAfter = new HashSet<>();
                SetOfAreasPatrolledAfter.addAll(SetOfAreasPatrolledNow);
                SetOfAreasPatrolledAfter.addAll(GetNumberOfAreasPatrolled(i.toString()));
                if(maxNumberOfAreasPatrolled == 0){
                    newArea = i.toString();
                    maxNumberOfAreasPatrolled = SetOfAreasPatrolledAfter.size();
                }else if(SetOfAreasPatrolledAfter.size() > maxNumberOfAreasPatrolled){
                    newArea = i.toString();
                    maxNumberOfAreasPatrolled = SetOfAreasPatrolledAfter.size();
                }
                SetOfAreasPatrolledAfter.clear();
            }
        }


        // Getting  area to be removed
        Integer minIntersection = 0;
        for(Map.Entry<UUID, String> entry : listOfAreasPatrolled.entrySet()){
            Set<Integer> newAreaPatrolledSet = GetNumberOfAreasPatrolled(newArea);
            newAreaPatrolledSet.retainAll(GetNumberOfAreasPatrolled(entry.getValue()));
            Integer Intersection = newAreaPatrolledSet.size();
            if(minIntersection == 0){
                minIntersection = Intersection;
                removedArea = entry.getValue();
            }else if(minIntersection > Intersection){
                minIntersection = Intersection;
                removedArea = entry.getValue();
            }
        }


        // Check if its good to remove the area
        Set<Integer> SetOfAreasPatrolledAfterOptimization = new HashSet<>();

        for(Map.Entry<UUID, String> entry : listOfAreasPatrolled.entrySet()){
            if(!entry.getValue().equals(removedArea)){
                SetOfAreasPatrolledAfterOptimization.addAll(GetNumberOfAreasPatrolled(entry.getValue()));
            }
        }

        SetOfAreasPatrolledAfterOptimization.addAll(GetNumberOfAreasPatrolled(newArea));

        if(SetOfAreasPatrolledAfterOptimization.size() > SetOfAreasPatrolledNow.size()){
            Optimization newOptimization = new Optimization(0f, removedArea, newArea);
            return newOptimization;
        }else{
            Optimization newOptimization = new Optimization(0f, newArea, newArea);
            return newOptimization;
        }

    }*/

    // Changed 2: NOOP1
    /*public Optimization GetPossibleOptimization(Map<UUID, String> listOfAreasPatrolled){

        Float UnCoverageIndex = 0f;
        String areaRemoved = "";
        String areaAdded = "";

        Float maxMedia = 0f;
        Float maxDistance = 0f;

        try{
            for (Integer i = 1; i<=numOfAreas; i++){
                Float distanceMedia = 0f;
                for (Map.Entry<UUID, String> entry : listOfAreasPatrolled.entrySet()) {
                    // UnCoverageIndex
                    UnCoverageIndex += distance(i.toString(), entry.getValue());
                    // area Added
                    distanceMedia += distance(i.toString(), entry.getValue());
                }
                // area Added
                if(distanceMedia > maxMedia){
                    maxMedia = distanceMedia;
                    areaAdded = i.toString();
                }

            }
        }catch (Exception e){
            e.printStackTrace();
        }

        // area Removed

        // 1st check if there are patrols in more than one area
        try{
            Set<String> uniqueValues = new HashSet<String>(listOfAreasPatrolled.values());
            Integer maxCountArea = 1;
            for(String area: uniqueValues){
                Integer countArea = 0;
                for (Map.Entry<UUID, String> entry : listOfAreasPatrolled.entrySet()) {
                    if(area.equals(entry.getValue())){
                        countArea += 1;
                    }
                    if(countArea > maxCountArea){
                        maxCountArea = countArea;
                        areaRemoved = area;
                    }
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }

        if(areaRemoved.equals("")){
            try{
                for (Map.Entry<UUID, String> entry : listOfAreasPatrolled.entrySet()) {
                    Float distanceAllAreas = 0f;
                    for (Integer i = 1; i<=numOfAreas; i++){
                        distanceAllAreas += distance(entry.getValue(),i.toString());
                    }

                    if(maxDistance == 0){
                        maxDistance = distanceAllAreas;
                        areaRemoved = entry.getValue();
                    }
                    else if(distanceAllAreas > maxDistance){
                        maxDistance = distanceAllAreas;
                        areaRemoved = entry.getValue();
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }

            // Check if its worth
            Float distanceAreaRemoved = 0f;
            try{

                Integer areaRemovedInt = Integer.parseInt(areaRemoved);

                for (Map.Entry<UUID, String> entry : listOfAreasPatrolled.entrySet()) {
                    distanceAreaRemoved += distance(areaRemovedInt.toString(), entry.getValue());
                }

                distanceAreaRemoved += distance(areaRemovedInt.toString(), areaAdded);

            }catch (Exception e){
                e.printStackTrace();
            }

            if(distanceAreaRemoved > maxMedia){
                Optimization newOptimization = new Optimization(UnCoverageIndex, areaRemoved, areaRemoved);
                return newOptimization;
            }else{
                Optimization newOptimization = new Optimization(UnCoverageIndex, areaRemoved, areaAdded);
                return newOptimization;
            }
        }else{
            Optimization newOptimization = new Optimization(UnCoverageIndex, areaRemoved, areaAdded);
            return newOptimization;

        }
    }*/

    // Changed 2: NOOP2
    public Optimization GetPossibleOptimization(Map<UUID, String> listOfAreasPatrolled){

        Float UnCoverageIndex = 0f;
        String areaRemoved = "";
        String areaAdded = "";

        Float maxMedia = 0f;
        Float maxDistance = 0f;

        try{
            for (Integer i = 1; i<=numOfAreas; i++){
                Float distanceMedia = 0f;
                for (Map.Entry<UUID, String> entry : listOfAreasPatrolled.entrySet()) {
                    // UnCoverageIndex
                    UnCoverageIndex += distance(i.toString(), entry.getValue());
                    // area Added
                    distanceMedia += distance(i.toString(), entry.getValue());
                }
                // area Added
                if(distanceMedia > maxMedia){
                    maxMedia = distanceMedia;
                    areaAdded = i.toString();
                }

            }
        }catch (Exception e){
            e.printStackTrace();
        }

        // area Removed

        // 1st check if there are patrols in more than one area
        try{
            Set<String> uniqueValues = new HashSet<String>(listOfAreasPatrolled.values());
            Integer maxCountArea = 1;
            for(String area: uniqueValues){
                Integer countArea = 0;
                for (Map.Entry<UUID, String> entry : listOfAreasPatrolled.entrySet()) {
                    if(area.equals(entry.getValue())){
                        countArea += 1;
                    }
                    if(countArea > maxCountArea){
                        maxCountArea = countArea;
                        areaRemoved = area;
                    }
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }

        if(areaRemoved.equals("") && !areaAdded.equals("")){
            try{
                Float minimumDistanceToPatrol = -1f;
                for (Map.Entry<UUID, String> entry : listOfAreasPatrolled.entrySet()) {

                    Float distanceToPatrol = distance(entry.getValue(), areaAdded);

                    if(minimumDistanceToPatrol < 0){
                        minimumDistanceToPatrol = distanceToPatrol;
                        areaRemoved = entry.getValue();
                    }

                    if(distanceToPatrol < minimumDistanceToPatrol){
                        minimumDistanceToPatrol = distanceToPatrol;
                        areaRemoved = entry.getValue();
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }

            // Check if its worth
            Float distanceAreaRemoved = 0f;
            try{

                Integer areaRemovedInt = Integer.parseInt(areaRemoved);

                for (Map.Entry<UUID, String> entry : listOfAreasPatrolled.entrySet()) {
                    distanceAreaRemoved += distance(areaRemovedInt.toString(), entry.getValue());
                }

                distanceAreaRemoved += distance(areaRemovedInt.toString(), areaAdded);

            }catch (Exception e){
                e.printStackTrace();
            }

            if(distanceAreaRemoved > maxMedia){
                Optimization newOptimization = new Optimization(UnCoverageIndex, areaRemoved, areaRemoved);
                return newOptimization;
            }else{
                Optimization newOptimization = new Optimization(UnCoverageIndex, areaRemoved, areaAdded);
                return newOptimization;
            }
        }else{
            Optimization newOptimization = new Optimization(UnCoverageIndex, areaRemoved, areaAdded);
            return newOptimization;

        }
    }

    // Calculate a new Patrol Area (returns AreaID)

    // Change 1: NOOP3
    /*public String CalculateNewPatrolArea(Map<UUID, DefaultPatrol> map){

        String AreaID = "1";
        Integer maxNumberOfAreasPatrolled = 0;

        if(map != null) {
            if (map.size() > 0) {

                Map<UUID, String> listOfAreasPatrolled = GetListOfAreasPatrolled(map);

                Set<Integer> SetOfAreasPatrolledNow = new HashSet<>();

                for(Map.Entry<UUID, String> entry : listOfAreasPatrolled.entrySet()){
                    SetOfAreasPatrolledNow.addAll(GetNumberOfAreasPatrolled(entry.getValue()));
                }

                // Iterate Areas to find the best
                for (Integer i = 1; i<=numOfAreas; i++){
                    if(!listOfAreasPatrolled.containsValue(i.toString())){
                        Set<Integer> SetOfAreasPatrolledAfter = new HashSet<>();
                        SetOfAreasPatrolledAfter.addAll(SetOfAreasPatrolledNow);
                        SetOfAreasPatrolledAfter.addAll(GetNumberOfAreasPatrolled(i.toString()));
                        if(maxNumberOfAreasPatrolled == 0){
                            AreaID = i.toString();
                            maxNumberOfAreasPatrolled = SetOfAreasPatrolledAfter.size();
                        }else if(SetOfAreasPatrolledAfter.size() > maxNumberOfAreasPatrolled){
                            AreaID = i.toString();
                            maxNumberOfAreasPatrolled = SetOfAreasPatrolledAfter.size();
                        }
                        SetOfAreasPatrolledAfter.clear();
                    }
                }
            }
        }

        return AreaID;
    }*/

    // Changed 1: NOOP1 - NOOP2
    /*public String CalculateNewPatrolArea(Map<UUID, DefaultPatrol> map){

        String AreaID = "1";
        Float maxMedia = 0f;

        if(map != null) {
            if (map.size() > 0) {

                Map<UUID, String> listOfAreasPatrolled = GetListOfAreasPatrolled(map);

                // Iterate Areas to find medium distance to Patrols
                for (Integer i = 1; i<=numOfAreas; i++){
                    Float distanceMedia = 0f;
                    for (Map.Entry<UUID, String> entry : listOfAreasPatrolled.entrySet()) {
                        distanceMedia += distance(i.toString(), entry.getValue());
                    }
                    distanceMedia = (distanceMedia/map.size());
                    if(distanceMedia > maxMedia){
                        maxMedia = distanceMedia;
                        AreaID = i.toString();
                    }
                }
            }
        }

        return AreaID;
    }*/

    // Changed 1: DEFAULT - GROUP
    public String CalculateNewPatrolArea(Map<UUID, DefaultPatrol> map){

        Integer areaInt = ThreadLocalRandom.current().nextInt(1, numOfAreas);

        return areaInt.toString();
    }


    public Set<Integer> GetNumberOfAreasPatrolled(String Area){

        Integer groupID = 0;
        Set<Integer> AreasPatrolled = new HashSet<>(100);

        try{

            List<Integer> prePositionsList = new ArrayList<>(100);

            Integer areasPerSide = GetAreasPerSide();

            groupID = Integer.parseInt(Area);
            prePositionsList.add(groupID);

            Integer groupIDRight = groupID + 1;
            if(((groupID-1)/areasPerSide) == ((groupIDRight-1)/areasPerSide)){
                prePositionsList.add(groupIDRight);
            }

            Integer groupIDLeft = groupID - 1;
            if(((groupID-1)/areasPerSide) == ((groupIDLeft-1)/areasPerSide)){
                prePositionsList.add(groupIDLeft);
            }

            Integer groupIDUP = groupID + GetAreasPerSide();
            prePositionsList.add(groupIDUP);

            Integer groupIDUPRight =  groupIDUP + 1;
            if(((groupIDUP-1)/areasPerSide) == ((groupIDUPRight-1)/areasPerSide)){
                prePositionsList.add(groupIDUPRight);
            }

            Integer groupIDUPLeft =  groupIDUP - 1;
            if(((groupIDUP-1)/areasPerSide) == ((groupIDUPLeft-1)/areasPerSide)){
                prePositionsList.add(groupIDUPLeft);
            }

            Integer groupIDDOWN = groupID - GetAreasPerSide();
            prePositionsList.add(groupIDDOWN);

            Integer groupIDDOWNRight =  groupIDDOWN + 1;
            if(((groupIDDOWN-1)/areasPerSide) == ((groupIDDOWNRight-1)/areasPerSide)){
                prePositionsList.add(groupIDDOWNRight);
            }

            Integer groupIDDOWNLeft =  groupIDDOWN - 1;
            if(((groupIDDOWN-1)/areasPerSide) == ((groupIDDOWNLeft-1)/areasPerSide)){
                prePositionsList.add(groupIDDOWNLeft);
            }


            for(Integer j: prePositionsList){
                if( j > 0 && j <= this.GetNumOfAreas()){
                    AreasPatrolled.add(j);
                }
            }

        }catch (Exception e){
            return AreasPatrolled;
        }

        return AreasPatrolled;
    }

    // Calculate UnCoverageIndex (smaller the best)
    public Float GetUnCoverageIndex(Map<UUID, String> listOfAreasPatrolled){
        Float UnCoverageIndex = 0f;

        if(listOfAreasPatrolled != null) {
            if (listOfAreasPatrolled.size() > 0) {

                // Calculate List of Area to Area with Patrol Distances
                for (Integer i = 1; i<=numOfAreas; i++){
                    for (Map.Entry<UUID, String> entry : listOfAreasPatrolled.entrySet()) {
                        UnCoverageIndex += distance(i.toString(), entry.getValue());
                    }
                }
            }
        }

        return UnCoverageIndex;
    }


    public Float distance(Area L1, Area L2){

        Float distance = 0f;
        try {
            Float X = (float) Math.pow((L1.getLat() - L2.getLat()), 2);
            Float Y = (float) Math.pow((L1.getLon() - L2.getLon()), 2);

            distance = (float) Math.pow((X + Y), 0.5);
        }catch (Exception e){
            e.printStackTrace();
        }
        return distance;
    }


    public Float distance(String S1, String S2){

        Area L1 = this.mapOfAreas.get(S1);
        Area L2 = this.mapOfAreas.get(S2);

        Float distance = 0f;
        try {
            Float X = (float) Math.pow((L1.getLat() - L2.getLat()), 2);
            Float Y = (float) Math.pow((L1.getLon() - L2.getLon()), 2);

            distance = (float) Math.pow((X + Y), 0.5);
        }catch (Exception e){
            e.printStackTrace();
        }
        return distance;
    }


    public Map<UUID, String> GetListOfAreasPatrolled(Map<UUID, DefaultPatrol> map){

        Map<UUID, String> listOfAreasPatrolled = new HashMap<UUID, String>();

        try {
            if(map.size() > 0){
                for (Map.Entry<UUID, DefaultPatrol> entry : map.entrySet()) {
                    if (entry.getValue().position.equals(PatrolPosition.ToArea.toString()) ||
                            entry.getValue().position.equals(PatrolPosition.OnArea.toString())) {
                        listOfAreasPatrolled.put(entry.getKey(), entry.getValue().area);
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return listOfAreasPatrolled;
    }


    public class Optimization{
        public Float unCoverageIndex;
        public String areaRemoved;
        public String areaAdded;

        public Optimization(Float UnCoverageIndex, String AreaRemoved, String AreaAdded){
            this.unCoverageIndex = UnCoverageIndex;
            this.areaRemoved = AreaRemoved;
            this.areaAdded = AreaAdded;
        }
    }


    public Area GetAreaFromLocation(Location L){

        for (Map.Entry<String, Area> entry : mapOfAreas.entrySet()){
            if(entry.getValue().InArea(L)){
                return entry.getValue();
            }
        }

        return null;
    }


    public Location GetLocationFromArea(String Area){
        return  this.mapOfAreas.get(Area).GetLocation();
    }


    public Float MaxX(){
        return this.maxX;
    }


    public Float MaxY(){
        return this.maxY;
    }


}
