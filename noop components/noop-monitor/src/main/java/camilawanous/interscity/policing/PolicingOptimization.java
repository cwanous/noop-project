package camilawanous.interscity.policing;

import camilawanous.interscity.data.MonitorPatrol;
import camilawanous.interscity.data.Occurrence;
import camilawanous.noopdata.Location;
import lac.cnclib.net.groups.Group;

import java.util.*;

public class PolicingOptimization {

    public PolicingOptimization(){
    }

    public List<Group> OccurrencePatrolAreas(Occurrence occcurrence){
        List<Group> areasList = new ArrayList<>(1000);

        Group group = new Group(3,1);

        areasList.add(group);

        return areasList;
    }

    public List<Group> OccurrencePatrolAreas(){
        List<Group> areasList = new ArrayList<>(1000);

        Group group = new Group(3,1);

        areasList.add(group);

        return areasList;
    }


    public String CalculateArea(){
        return "1";
    }

    public List<MonitorPatrol> ReCalculateAreaToAllPatrols(List<MonitorPatrol> monitorPatrolList){

        List<MonitorPatrol> newAreas = new ArrayList<MonitorPatrol>();

        // Logic that recalculate Patrols Areas (only the ones that are not {ToOccurrence, OnOccurrence})
        for (MonitorPatrol entry : monitorPatrolList){
            entry.area = "2";
            Location location = new Location(2f,2f);
            entry.location = location;
            newAreas.add(entry);
        }

        return newAreas;
    }

    public void UpdatePosition(UUID PatrolID, String Position){

    }

}
