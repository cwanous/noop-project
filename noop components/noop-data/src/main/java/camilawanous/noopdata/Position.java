package camilawanous.noopdata;

public enum Position{
    ToArea,
    OnArea,
    ToOccurrence,
    OnOccurrence;
}