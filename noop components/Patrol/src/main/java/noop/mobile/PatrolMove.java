package noop.mobile;

import camilawanous.noopdata.Area;
import camilawanous.noopdata.Location;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PatrolMove {

    private static final int maximumPoolSize = Runtime.getRuntime().availableProcessors() * 2;
    private final ExecutorService threadPool;
    private Patrol patrol;
    private Location realTimeLocation;
    private CountDownLatch latch;

    private String area;
    private Boolean keepArea;
    private PatrolMoveTask moveTask;
    private Boolean onMove;

    public PatrolMove(Patrol Patrol){
        this.threadPool = Executors.newFixedThreadPool(maximumPoolSize);
        this.patrol = Patrol;
        this.realTimeLocation = new Location();
        this.onMove = false;
    }

    public PatrolMove(){
        this.threadPool = Executors.newFixedThreadPool(maximumPoolSize);
    }

    public void NewPatrolMoveTask(String AreaID){

        synchronized (this){
            Stop();
        }

        // get the Area
        Area area = this.patrol.GetCityArea().mapOfAreas.get(AreaID);
        System.out.println("# Start Moving to Area " + area.toString());

        this.latch = new CountDownLatch(1);
        moveTask = new PatrolMoveTask(area, this, latch);
        this.onMove = true;

        // move to Area
        this.threadPool.execute(moveTask);
    }

    public synchronized void Stop(){
        try {
            if(this.moveTask != null){
                if(this.onMove){
                    System.out.println("### STOP Patrol Move ###");
                    this.moveTask.Interrupted();
                    this.moveTask.terminate();
                    latch.await();
                    System.out.println("### Finally STOP ###");
                }
            }
        } catch (InterruptedException e) {
            System.out.println("# Start Policing");
        }
    }

    // To get Location in real time
    public Location GetRealTimeLocation(){
        return this.realTimeLocation;
    }

    public void Arrives(){
        this.onMove = false;
        this.patrol.Arrives();

    }

    public void PatrolMoveStop(){
        this.onMove = false;
    }

    public void ChangeMoveArea(String AreaID){
        try {
            if(moveTask.isAlive()){
                Area area = this.patrol.GetCityArea().mapOfAreas.get(AreaID);
                this.moveTask.ChangeArea(area);
            }else {
                NewPatrolMoveTask(AreaID);
            }
        }catch (Exception e){

        }
    }

}
